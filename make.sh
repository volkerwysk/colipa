#! /bin/bash

# This script calls the Mercury compiler for compiling a program. Specify the program's main module. The default is
# "test".

PROG="$1"

if [ "$PROG" == "" ] ; then
    PROG=test
fi

mmc -j16 -E --make --output-compile-error-lines=200 \
    --error-files-in-subdir \
    --no-color-diagnostics \
    --output-compile-error-lines=200 \
    --max-error-line-width=115 \
    `pkg-config --libs glib-2.0` \
    --cflags "$(pkg-config --cflags glib-2.0)" \
    "$PROG" --grade asm_fast.gc.debug.stseg 


# Im Programm "ohneargs" wird das Werfen der Ausnahme WEGOPTIMIERT, wenn folgende Parameter verwendet werden:
#    -O6 --no-fully-strict

etags colipa.m test.m ade_test.m

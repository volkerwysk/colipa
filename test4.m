% Test for text layout
:- module test4.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.

:- implementation.
:- import_module list, int, string, char, bool, assoc_list, pair, maybe, require.
:- import_module colipa, unicode.



main(!IO) :-
    io.write_string("----------------------------------------\n", !IO),

    Text1 = "       This  is some   text with             weird spaces       " ++
            "and an indentation    at the beginning of the line. " ++
            "\nAnd it has an unindented second line. And there is an " ++
            "overlongoverlongoverlongoverlongoverlongoverlongoverlongoverlong word.",
    layout_text(0, 40, Text1, Str1),
    io.write_string(Str1, !IO),
    io.nl(!IO), io.nl(!IO),

    Text2 = "This is some text without weird spaces " ++
            "and an indentation at the beginning of the line.\n",
    layout_text(0, 40, Text2, Str2),
    io.write_string(Str2, !IO),
    io.nl(!IO), io.nl(!IO),

    Text3 = "This is some text with Chinese wide characters, which I got from a spam mail: " ++
            "价格优惠，尤其非洲南美南太平洋东欧等经济不发达国家价格更优惠",
    layout_text(0, 40, Text3, Str3),
    io.write_string(Str3, !IO),
    io.nl(!IO), 
    io.write_string("----------------------------------------\n", !IO).

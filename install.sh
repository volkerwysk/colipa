#! /bin/bash

time mmc --make -j16 \
    --mercury-linkage shared \
    --error-files-in-subdir \
    --cflags "$(pkg-config --cflags glib-2.0)" \
    libcolipa.install

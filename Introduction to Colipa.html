<h1 class="no_toc" id="introduction-to-colipa">Introduction to Colipa</h1>

<ul id="markdown-toc">
  <li><a href="#gnu-style-command-line-arguments" id="markdown-toc-gnu-style-command-line-arguments">GNU style command line arguments</a></li>
  <li><a href="#introduction" id="markdown-toc-introduction">Introduction</a>    <ul>
      <li><a href="#defining-the-arguments" id="markdown-toc-defining-the-arguments">Defining the arguments</a></li>
      <li><a href="#parsing-the-command-line" id="markdown-toc-parsing-the-command-line">Parsing the command line</a></li>
      <li><a href="#extracting-information-from-the-command-line" id="markdown-toc-extracting-information-from-the-command-line">Extracting information from the command line</a></li>
      <li><a href="#errors-and-error-messages" id="markdown-toc-errors-and-error-messages">Errors and error messages</a></li>
      <li><a href="#automatically-generated-usage-information" id="markdown-toc-automatically-generated-usage-information">Automatically generated usage information</a></li>
      <li><a href="#new-argument-types" id="markdown-toc-new-argument-types">New argument types</a></li>
      <li><a href="#group-constraints" id="markdown-toc-group-constraints">Group Constraints</a></li>
      <li><a href="#checkers" id="markdown-toc-checkers">Checkers</a></li>
      <li><a href="#command-line-evaluation-framework-for-programs-with-subcommands" id="markdown-toc-command-line-evaluation-framework-for-programs-with-subcommands">Command line evaluation framework for programs with subcommands</a></li>
    </ul>
  </li>
</ul>

<p>This is an introduction on how to use Colipa. The full documentation is in the comments in the interface section of
the <code>colipa.m</code> file. You can also find an example program in the <code>test.m</code> file.</p>

<p>This file is released under the terms of the GNU Free Documentation License, Version 1.3 or any later version. See
COPYING.FDL for details.</p>

<h2 id="gnu-style-command-line-arguments">GNU style command line arguments</h2>

<p>Colipa supports GNU style command line arguments. This means you have short arguments, which begin with a single
dash (<code>-a</code>) and long arguments, which begin with a double dash (<code>--arg</code>). Both can have a value, which can be
placed inside the parameter or in the next parameter (<code>-aval</code>, <code>--arg=val</code>, <code>-a val</code> or <code>--arg val</code>). Parameters
can be arguments (beginning with a dash) or non-arguments (everything else). Arguments can occur anywhere in the
command line, not just at the beginning. Everything after the special parameter <code>--</code> is treated as a non-argument.
Long arguments can be abbreviated as long as they are unique.</p>

<h2 id="introduction">Introduction</h2>

<h3 id="defining-the-arguments">Defining the arguments</h3>

<p>Command line arguments are identified by unique strings. These strings are semantically wrapped inside the <code>cla</code>
type. This makes it possible to reuse and share command line argument definitions. (Previously, in the 1 versions
of Colipa, the arguments were identified by an enumeration type. That made it impossible to define command line
arguments in one module and use them in another.)</p>

<p>This is the type of the command line argument identifiers:</p>

<pre><code>:- type cla ---&gt; cla(cla :: string).
</code></pre>

<p>The argument identifiers have the form <code>cla(Str)</code>, where <code>Str</code> is a unique string that names the argument. It is
meant not to change, although it’s relatively easy to change it later - you just need to replace all occurences of
<code>cla(XXX)</code> with <code>cla(YYY)</code>, where <code>XXX</code> and <code>YYY</code> are the strings which identify the argument, before and after the
change.</p>

<p>You describe each command line argument with a list of properties. You have one big list of all the arguments and
their properties. The required type for that list is <code>assoc_list(cla, list(property))</code>. For instance:</p>

<pre><code>:- func argdescl = assoc_list(cla, list(property)).

argdescl = [
        cla("dir") -
        [ prop_long("dir"),
          prop_switch_value("Dir"),
          prop_description("Name of the directory.")
        ],

        cla("archive") -
        [ prop_short('a'),
          prop_long("archive"),
          prop_switch,
          prop_description("If the program is to archive the data.")
        ],

        ...
].

</code></pre>

<p>All arguments need to have a name, by which they appear on the command line. It can be either short (<code>prop_short</code>)
or long (<code>prop_long</code>) or both. If you want to use the automatic usage info generation, you need to give your
arguments descriptions, with the <code>prop_description</code> property. See <code>colipa.m</code> for a list of all supported
properties.</p>

<p>The <code>prop_default</code> property, for setting a default value, is different. It must be specified like this:</p>

<pre><code>'new prop_default'(Default)
</code></pre>

<p>The <code>new </code> prefix is needed because the data constructor uses an existential type. See the chapter
<a href="https://mercurylang.org/information/doc-release/mercury_ref/Existential-types.html">Existential types</a> in
the Mercury Language Reference Manual. But don’t worry, you don’t need to understand existential types for using
Colipa.</p>

<h3 id="parsing-the-command-line">Parsing the command line</h3>

<p>Next, you need to call the command line parser. The most simple way should be to use the <code>colipa/5</code> predicate:</p>

<pre><code>:- pred colipa(
    assoc_list(cla, list(property))::in,
    arguments::out,
    list(string)::out,
    io::di, io::uo)
is det.
</code></pre>

<p>You pass it, as the first parameter, your argument description list. The recognized command line arguments are
returned in the second parameter, in a value of type <code>arguments</code>. Later you use it when querying the command line
arguments. The non-argument parameters are returned in the third parameter of <code>colipa/5</code>. So the call looks like
this:</p>

<pre><code>colipa(argdescl, Arguments, NonArguments, !IO)
</code></pre>

<h3 id="extracting-information-from-the-command-line">Extracting information from the command line</h3>

<p>Now you’re ready to get the information you want out of the parsed command line arguments. This is done by <em>query
functions</em>. The function to use is determined by what you want to get out of the parameters. For instance, for an
argument that can be specified zero or one times, and has a value, you would use the <code>arg_maybe_value</code> function. If
your argument is of type <code>T</code>, then <code>arg_maybe_value</code> will return a value of type <code>maybe(T)</code>. When the argument
hasn’t been given on the command line, it would return <code>no</code>. If it has, it would return <code>yes(Value)</code>.</p>

<p>The used query function must match the argument definition (the argument’s property list). For instance, you can’t
query an argument that doesn’t take a value, with <code>arg_value</code> or <code>arg_maybe_value</code>. It is probably a bug, when this
should happen. If such a mismatch is detected, an <code>arg_desc_error</code> is thrown. You can catch and output that error
and get an elaborate message about what went wrong.</p>

<p>The query functions all take the <code>Arguments</code> value and the argument identifier as parameters, and return the
queried value. Using a query function could, for instance, look like this:</p>

<pre><code>Dir = arg_maybe_value(Arguments, cla("dir"))
</code></pre>

<p>When the type of the argument value isn’t clear from the context, you will get a compile time error. In this case,
you need to explicitly specify the type. Like this:</p>

<pre><code>Dir = arg_maybe_value(Arguments, cla("dir")) : maybe(string)
</code></pre>

<h3 id="errors-and-error-messages">Errors and error messages</h3>

<p>The user can make errors. For instance, supplying a value to an argument that doesn’t take one, or supplying a
value that isn’t a well-formed, parseable representation of the argument’s type. The programmer can make errors as
well - bugs. For instance, by defining an argument with contradictory properties, or by querying an argument with a
query function for the wrong type. Not all programmer errors can be detected at compile time.</p>

<p>This means that there are two types, which can be thrown by Colipa: <code>command_line_error</code> for errors that the user
makes, and <code>arg_desc_error</code> for errors by the programmer. Both can be triggered by the <code>colipa/5</code> predicate and
by a query function.</p>

<p>You catch them like this:</p>

<pre><code>try [io(!IO)] ...
then ...
catch (ADE : arg_desc_error) -&gt;
    print_on_terminal("Bug found: " ++ ade_message(ADE) ++ "\n", !IO)
catch (CLE : command_line_error) -&gt;
    print_on_terminal(cle_message(CLE) ++ "\n", !IO).
</code></pre>

<p>You can also catch individual errors like this:</p>

<pre><code>catch (cle_missing_value(ArgDesc, No, Last) : command_line_error) -&gt;
    ...
</code></pre>

<p>The <code>print_on_terminal</code> predicate determines the width of the terminal (80 is used if not connected to a terminal)
and wraps lines such that no words are split up. <code>cle_message/1</code> and <code>ade_message/1</code> make an error message, from
a <code>command_line_error</code> or <code>arg_desc_error</code>, respectively.</p>

<p>The predicate which catches the exceptions must have the <code>cc_multi</code> determinism category. See <a href="https://mercurylang.org/information/doc-release/mercury_ref/Exception-handling.html">Exception
Handling</a> in the Mercury
Language Reference Manual.</p>

<h3 id="automatically-generated-usage-information">Automatically generated usage information</h3>

<p>Usage information about the command line arguments can be generated automatically from the argument descriptions.
Use the <code>prop_description</code> property to add a descriptive text. This text should not be broken down into lines by
newline characters. Colipa does that by itself. But newlines are allowed and will be respected.</p>

<p>To print a usage summary on stdout, use this predicate:</p>

<pre><code>print_usage_info(argdescl, !IO)
</code></pre>

<p>There are more and more flexible predicates to do this. You can also sort the argument list by the name of the
first long argument:</p>

<pre><code>sort_argdescl(argdescl, ArgDescL2),
print_usage_info(ArgDescL2, !IO)
</code></pre>

<p>Here’s an example of what it looks like, in a terminal of width 58. Color isn’t shown here:</p>

<pre><code>-v                 This argument can be given up to three
                   times.
-a  --archive      This is a switch. It can be specified
                   zero or one times. It's value is a
                   boolean.
-c  --count Count  This argument takes an integer. It
                   also has a default and a checker.
    --dir Dir      Name of the directory.
    --float F      This is an example of an argument that
                   takes a floating point number as its
                   value.
...
</code></pre>

<h3 id="new-argument-types">New argument types</h3>

<p>By default, Colipa supports the argument types <code>string</code>, <code>int</code> and <code>float</code>. You can add new argument types easily
by instantiating the <code>argument</code> typeclass. You need to define a parser for that type, which goes into the
<code>from_str</code> method. It will return <code>fs_ok(Val)</code> for a successful parse, and <code>fs_error(Msg)</code> for a parse error, where
<code>Msg</code> is an error message. When the parse failed, Colipa will throw a <code>cle_malformed_value</code> exception, which
includes the error message.</p>

<p>This is an example:</p>

<pre><code>:- instance argument(bool) where [
        from_str(Str) = Res :-
            ( if        Str = "yes" 
              then      Res = fs_ok(yes)

              else if   Str = "no" 
              then      Res = fs_ok(no)

              else      Res = fs_error("Expected ""yes"" or ""no"".")
            )
    ].
</code></pre>

<h3 id="group-constraints">Group Constraints</h3>

<p>While argument descriptions can place constraints and checks on individual arguments, they can’t place restrictions
on groups of arguments. For this purpose, group constraints predicates have been added. They begin with <code>args_</code>.
For instance, the args_one/3 predicate checks if exactly one of a group of arguments is provided on the command
line. They each check something about several arguments and throw a <code>command_line_error</code> if the constraint isn’t
observed.</p>

<h3 id="checkers">Checkers</h3>

<p>Checkers check an argument value from the command line, after it has been successfully parsed. This means, they
place additional constraints on a command line argument. For instance, there are two predefined checkers in Colipa,
called <code>nonneg_integer</code> and <code>positive_integer</code>, respectively. They check an integer for being non-negative or
positive (one or bigger). When a checker fails, a <code>cle_invalid_value</code> error is thrown.</p>

<p>You can define a checker with the <code>make_checker</code> function. For instance, <code>nonneg_integer</code> is defined like this:</p>

<pre><code>nonneg_integer = make_checker(nni).

:- pred nni(int::in, maybe(string)::out) is det.

nni(I, Err) :-
    (  if I &lt; 0 then Err = yes("Non-negative integer expected.")
                else Err = no ).
</code></pre>

<p><code>make_checker</code> takes a predicate, which checks the value and returns an error message in case the check failed.
This message is included in the <code>cle_invalid_value</code> exception. The type of <code>make_checker</code> is:</p>

<pre><code>:- func make_checker(
      pred(T, maybe(string))::pred(in, out) is det
      ) = (checker::out) is det
      &lt;=  argument(T).
</code></pre>

<p>Checkers are added to a command line argument with the <code>prop_check</code> property, such as <code>prop_check(nonneg_integer)</code>.</p>

<h3 id="command-line-evaluation-framework-for-programs-with-subcommands">Command line evaluation framework for programs with subcommands</h3>

<p>Colipa provides a simple framework for dealing with command line arguments and subcommands.</p>

<p>The <code>program/6</code> predicate strives to unburden you when your program uses a subcommand. It is passed a stripped down
version of your main predicate. Part of the subcommand and command line handling is done by <code>program/6</code>, so you
don’t have to do it there. It passes the following pieces to your new main predicate:</p>

<ul>
  <li>The subcommand as chosen on the command line. This is always valid. An invalid subcommand is caught by 
<code>program/6</code> and it exits which an error message.</li>
  <li>The compiled command line arguments. <code>colipa/5</code> or <code>colipa1/5</code> don’t need to be called by the programmer any
longer.</li>
  <li>The list of non-arguments. The chosen subcommand has been removed from it.</li>
</ul>

<p>It handles the special subcommand “help” and prints usage information (including subcommands and command line
arguments). It also handles invalid subcommands. If no subcommand is specified, a short message is printed, which
tells the user to call the program with “help”.</p>

<p>See <code>colipa.m</code> for the invocation details of <code>program/6</code>.</p>

<p>The framework also provides a predicate <code>catch_cle_ade/5</code>:</p>

<pre><code>:- pred catch_cle_ade(
    int::in,                                    % Exit code for command line error
    int::in,                                    % Exit code for argument description error
    pred(io, io)::in(pred(di, uo) is cc_multi), % Predicate to wrap
    io::di, io::uo
) is cc_multi.
</code></pre>

<p>This wraps some IO predicate and deals with <code>command_line_error</code>s and <code>arg_desc_error</code>s by printing an error
message to stderr and signaling the program to exit. This signaling is done by throwing a value of this type:</p>

<pre><code>:- type shutdown ---&gt; shutdown(int).
</code></pre>

<p>The enclosed integer is the intended exit code of the program. The top-level main program is meant to catch such
shutdown exceptions, set the exit code and leave. Like this:</p>

<pre><code>main(!IO) :-
    try [io(!IO)]
        main1(!IO)
    then true
    catch (colipa.shutdown(ExitCode)) -&gt;
        io.set_exit_status(ExitCode, !IO).
</code></pre>

<p>The predicate <code>main1/2</code> here is the rest of the main program. It typically begins with a call to <code>catch_cle_ade/5</code>.</p>

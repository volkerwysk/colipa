% This is a test and sample program for the Colipa library. See the introduction for more information.

%--------------------------------------------------------------------------------------------------------------
% Copyright 2022-2024 Volker Wysk <post@volker-wysk.de>
% This file is distributed under the terms of the GNU Lesser General Public License, Version 3 or later. See
% the file COPYING.LESSER for details.
%--------------------------------------------------------------------------------------------------------------

:- module test.

:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is cc_multi.

:- implementation.
:- import_module list, int, maybe, string, char, bool, map, pair, assoc_list, float, require.
:- import_module colipa.


:- func argdescl = assoc_list(cla, list(colipa.property)).

argdescl = [
        cla("dir") -
        [ prop_long("dir"), prop_short('d'), prop_switch_value("Dir"),
          prop_description("Name of the directory.")
        ],

        cla("mandatory") -
        [ prop_short('m'), prop_long("mandatory"), prop_value("Val"), prop_one,
          prop_description("This is an argument which must be specified. Colipa will generate a " ++
                           "cle_too_few_occurrences/3 error it it is missing.")
        ],

        cla("archive") -
        [ prop_short('a'), prop_long("archive"), prop_switch,
          prop_description("This is a switch. It can be specified zero or one times. It's value is a boolean.")
        ],

        cla("float") -
        [ prop_switch_value("F"), prop_long("float"),
          prop_description("This is an example of an argument that takes a floating point number as its value.")
        ],

        cla("target") -
        [ prop_switch_value("Target"), prop_long("target"),
          prop_description("This example will invalidate some of the abbreviations for --target-directory.")
        ],

        cla("targetdir") -
        [ prop_switch_value("Target"), prop_long("target-directory"), 'new prop_default'("/my/default/dir"),
          prop_description("This argument can be abbreviated, like most long arguments. And it has a default " ++
                           "value.")
        ],

        cla("manylong") -
        [ prop_switch, prop_long("long"), prop_long("long1"), prop_long("long2"), prop_long("long3"),
          prop_long("long4"),
          prop_description("These arguments names won't fit in the column of the argument names.")
        ],

        cla("verbose") -
        [ prop_short('v'), prop_max_times(3),
          prop_description("This argument can be given up to three times.")
        ],

        cla("count") -
        [ prop_long("count"), prop_short('c'), 'new prop_default'(1), prop_check(nonneg_integer),
          prop_switch_value("Count"), prop_env("COUNT"),
          prop_description("This argument takes an integer. It has a default, a checker and can be " ++
                           "specified with the environment variable COUNT")
        ],

        cla("count1") -
        [ prop_long("count1"), prop_check(positive_integer),
          prop_switch_value("Count"),
          prop_description("This argument takes a positive integer. This is done with the positive_integer " ++
                           "checker.")
        ],

        cla("tmpdir") -
        [ prop_long("tmpdir"), 'new prop_default'("/tmpdir/default/dir"), prop_value("Dir"), prop_one,
          prop_env("TMPDIR"),
          prop_description("This argument can also be set by using the TMPDIR environment variable. " ++
                           "It also has a default.")
        ],

        cla("tmpdir-nd") -
        [ prop_long("tmpdir-nd"), prop_switch_value("Dir"), prop_env("TMPDIRND"),
          prop_description("This argument can be set by using the TMPDIRND environment variable. It has no " ++
                           "default.")
        ],

        cla("foobar") -
        [ prop_long("foobar"), prop_short('f'), prop_value("Txt"),
          prop_description("This argument can be specified multiple times.")
        ],

        cla("mybool") -
        [ prop_long("mybool"), prop_switch_value("Bool"),
          prop_description("This argument uses the new argument type ""bool""")
        ],

        cla("x") -
        [ prop_long("disable-x"), prop_short('x'), prop_inverse_switch,
          prop_description("This argument is an inverse switch. It is turned on (with arg_switch), when it " ++
                           "NOT given on the command line.")
        ],

        cla("cache") -
        [ prop_long("cache"), prop_switch, prop_env("CACHE"),
          prop_description("This argument is a switch with an environment variable.")
        ],

        cla("invcache") -
        [ prop_long("invcache"), prop_inverse_switch, prop_env("INVCACHE"),
          prop_description("This argument is an inverse switch with an environment variable.")
        ]
].


:- func subcommands = assoc_list(string, string).

subcommands = [
    "test"     - "Do the Colipa command line parameters test.",
    "erklären" - "Explain something. (Only used for testing subcommands.)",
    "erneuern" - "Renewe something. (Only used for testing subcommands.)" 
].



% Let's define a new argument type.

:- instance argument(bool) where [
        from_str(Str) = Res :-
            ( if        Str = "yes"
              then      Res = fs_ok(yes)

              else if   Str = "no"
              then      Res = fs_ok(no)

              else      Res = fs_error("Expected ""yes"" or ""no""")
            )
    ].



main(!IO) :-
    try [io(!IO)]
        catch_cle_ade_extra(
            1, 2,
            program(
                subcommands,
                argdescl,
                (func(Progname) = "Colipa test program. Invocation:\n" ++ Progname ++
                                  " <Subcommand> [Arguments]\n"),
                main1),
            !IO)
    then true
    catch (shutdown(ExitCode)) ->
        io.set_exit_status(ExitCode, !IO).



:- pred main1(string::in, arguments::in, list(string)::in, io::di, io::uo) is cc_multi.

main1(Chosen, Args, NonArgs, !IO) :-
    (
        if
            Chosen = "test"
        then
            do_tests(Args, NonArgs, !IO)
        else if
            Chosen = "erklären"
        then
            io.write_string("Subcommand ""erklären"" selected.\n", !IO)
        else if
            Chosen = "erneuern"
        then
            io.write_string("Subcommand ""erneuern"" selected.\n", !IO)
            
        else unexpected($pred, "Unlisted subcommand found")
    ).


:- pred do_tests(
    arguments::in,      % Compiled command line arguments
    list(string)::in,   % Non-arguments
    io::di, io::uo
) is cc_multi.

do_tests(Arguments, NonArguments, !IO) :-
    io.write_string("Non-argument parameters: " ++ string(NonArguments) ++ "\n\n", !IO),

    % Place additional constraints
    args_one(Arguments, [cla("dir"), cla("target"), cla("targetdir")], no),
    args_at_most_one(Arguments, [cla("count"), cla("count1")], no),
    args_at_least_one(Arguments, [cla("x"), cla("mybool"), cla("count")], no),

    % Get the arguments
    query(Arguments, cla("dir"),             arg_maybe_value : val(maybe(string)), !IO),
    query(Arguments, cla("float"),           arg_maybe_value : val(maybe(float)),  !IO),
    query(Arguments, cla("mandatory"),       arg_value       : val(string),        !IO),
    query(Arguments, cla("archive"),         arg_switch      : val(bool),          !IO),
    query(Arguments, cla("target"),          arg_maybe_value : val(maybe(string)), !IO),
    query(Arguments, cla("targetdir"),       arg_value       : val(string),        !IO),
    query(Arguments, cla("manylong"),        arg_switch      : val(bool),          !IO),
    query(Arguments, cla("verbose"),         arg_times       : val(int),           !IO),
    query(Arguments, cla("count"),           arg_value       : val(int),           !IO),
    query(Arguments, cla("count1"),          arg_maybe_value : val(maybe(int)),    !IO),
    query(Arguments, cla("tmpdir"),          arg_value       : val(string),        !IO),
    query(Arguments, cla("tmpdir-nd"),       arg_maybe_value : val(maybe(string)), !IO),
    query(Arguments, cla("foobar"),          arg_values      : val(list(string)),  !IO),
    query(Arguments, cla("mybool"),          arg_maybe_value : val(maybe(bool)),   !IO),
    query(Arguments, cla("x"),               arg_switch      : val(bool),          !IO),
    query(Arguments, cla("cache"),           arg_switch      : val(bool),          !IO),
    query(Arguments, cla("invcache"),        arg_switch      : val(bool),          !IO).


:- type val(T) == (func(arguments, cla) = T).


:- pred query(
    arguments::in,
    cla::in,
    (func(arguments, cla) = T)::in(func(in, in) = out is det),
    io::di, io::uo)
is cc_multi.

query(Arguments, Arg, Query, !IO) :-
    try [io(!IO)]
        T = Query(Arguments, Arg)

    then io.format("%s: %s\n", [s(string(Arg)), s(string(T))], !IO)

    catch (CLE : command_line_error) ->
        shortened(CLE, CLEDesc),
        print_on_terminal(
            "\n" ++ string(Arg) ++ ": " ++ CLEDesc ++ ":\n\nCommand Line Error: " ++
            cle_message(CLE) ++ "\n", !IO).


:- module test2.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.

:- implementation.
:- import_module list, int, string, char, bool, assoc_list, pair, maybe.
:- import_module colipa.


main(!IO) :-

    try [io(!IO)]
        catch_cle_ade(
            1, 2,
            program(
                ["subc" - ""],
                argdescl,
                (func(Progname) = "Colipa test program. Invocation:\n" ++ Progname ++ 
                                  " <Subcommand> [Arguments]\n"),
                main2),
            !IO)
    then true
    catch (shutdown(ExitCode)) ->
        io.set_exit_status(ExitCode, !IO).


:- pred main2(string::in, colipa.arguments::in, list(string)::in, io::di, io::uo) is cc_multi.
:- pragma no_determinism_warning(pred(main2/5)).

main2(Action, Args, NonArgs, !IO) :-
    P = arg_maybe_value(Args, cla("fis:processes")) : maybe(int),
    io.format("Action=%s\nP=%s\nNonArgs=%s\n", [s(string(Action)), s(string(P)), s(string(NonArgs))], !IO).


:- func argdescl = assoc_list(cla, list(property)).

argdescl = [
        cla("fis:processes") -
        [ prop_long("processes"),
          prop_short('p'),
          prop_description("Number of worker processes to use. Defaults to the number of processor " ++
                           "cores (and hardware threads) available."),
          prop_switch_value("No"),
          prop_check(positive_integer)
        ]
].

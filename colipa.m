% Weiter: compile_argmaps erneuern

%====================================================================================================
% Colipa - Command Line Parser for Mercury
%====================================================================================================
%
% Copyright 2022-2024 Volker Wysk <post@volker-wysk.de>
% This file is distributed under the terms of the GNU Lesser General Public License, Version 3 or later. See
% the file COPYING.LESSER for details.
%--------------------------------------------------------------------------------------------------------------

:- module colipa.

% Terminology:
%
% The command line consists of a list of strings. Those strings are called PARAMETERS here. Some of the parameters
% begin with "-" or "--". I call them ARGUMENTS. An argument can have a VALUE. An argument can consist of one or
% two parameters. In the first case, the value is inside the paramter - "--arg=val" or "-aval". In the second case,
% the value is in a parameter of its own ("--arg", "val" or "-a", "val").
%
% Each of the command line arguments is defined by a list of PROPERTIES. The list as a whole or, respectively, the
% data structure argdesc/1, which is computed from that properties list, is called the argument DESCRIPTION.
%
% The arguments are identified by an enumeration type, which contains one constructor for each command line
% argument. These constructors are called IDENTIFIERS.
%
% QUERY FUNCTIONS are used to get typed values out of the command line. They check if the used query function
% matches the argument description. If not, an arg_desc_error exception is thrown. Different query functions query
% the arguments in different ways, and have different requirements on the argument descriptions. When a
% arg_desc_error is thrown, this usually means that a bug has been detected.
%
% When the command line paramters don't match the argument descriptions, a command_line_error is thrown.

%==============================================================================================================
:- interface.

:- import_module io, list, maybe, string, char, assoc_list, bool.
:- use_module type_desc.



% Contents:
%
% 1. Command line argument identifiers
% 2. Command line argument properties. A list of those define/describe a command line argument.
% 3. Processing the command line arguments
% 4. Argument query functions
% 5. Error handling
% 6. Argument value checkers
% 7. Usage info/help text generation


%--------------------------------------------------------------------------------------------------------------
% 1. Command line argument identifiers


% A command line argument identifier is a semantic wrapper of a string. The string uniquely identifies the
% argument. It may, but doesn't need to match the long or short argument name. It is meant not to change, although
% it's relatively easy to change it later - you just need to replace all occurences of "cla(Name1)" with
% "cla(Name2)", where "Name1" and "Name2" are the wrapped strings for the argument.

:- type cla ---> cla(cla :: string).



%--------------------------------------------------------------------------------------------------------------
% 2. Command line argument properties. A list of those define/describe a command line argument.

:- type property
   ---> % Long argument name. This is specified with a leading double dash on the command line.
        prop_long(string)

   ;    % Short argument name. This is specified with a leading single dash on the command line.
        prop_short(char)

   ;    % Specifies that the argument takes a value, and gives that value a name. The name is used in the
        % automatic usage info generation. Values can come from command line arguments, environment variables (see
        % prop_env) and defaults (see prop_default).
        prop_value(string)

   ;    % Default value for an argument which takes a value. This uses an existential type. That means that the
        % default must be specified like this in the property lists: 'new prop_default'(DefaultValue). The type
        % of the default will later be checked and an ade_type_mismatch_default error thrown, if it doesn't match
        % the type being queried by the query function. An argument which has a default will always have a value.
        some [T] prop_default(T)

   ;    % Minimum number of allowed occurences of the argument in the command line. When the minimum number isn't
        % set by prop_min_times or an other property, it defaults to zero.
        prop_min_times(int)

   ;    % Maximum number of allowed occurences of the argument in the command line. When the maximum number isn't
        % set by prop_max_times or an other property, it defaults to unlimited.
        prop_max_times(int)

   ;    % The argument is a switch. This means that it takes no value and is allowed to occur zero or one times in
        % the command line. The value is to be queried by arg_switch. The result is if the switch has been
        % specified on the command line.
        %
        % A switch (as well as an inverse switch) may have an environment variable (see prop_env). For an
        % environment variable called "VAR", the values "yes" and "1" specify that the value of the (inverse)
        % switch is "yes". The values "no" and "0" specify that the value of the switch is "no". It doesn't matter
        % if the switch is inverse. A present but empty value has the same effect as adding the switch to the
        % command line. In other words, it makes the argument value "yes" for an ordinary switch and "no" for an
        % inverse switch, provided the (inverse) switch doesn't occur on the command line.
        prop_switch

   ;    % The argument is an inverse switch. This means that it takes no value and is allowed to occur zero or one
        % times in the command line. The value is to be queried by arg_switch. The result is whether the (inverse)
        % switch is NOT specified. It switches something off. That's the inverse of a normal switch.
        %
        % An inverse switch may have an environment variable. For details see prop_switch.
        prop_inverse_switch

   ;    % The argument is a switch with a value. This means that it takes a value and is allowed to occur zero or
        % one times in the command line. When the argument has a default, it is to be queried with arg_value/2. If
        % it doesn't, query it with arg_maybe_value/2. The argument to prop_switch_value is a name for the value.
        % This name is used in the automatic usage info generation.
        prop_switch_value(string)

   ;    % The argument must occur once. When it has a value, this means, that it can come from the command line
        % (one occurence), an envionment variable (see prop_env) or a default value (see prop_default). So it must
        % occur in the command line only when it doesn't have a default and can't be specified via an environment
        % variable. Else, it is allowed no to occur in the command line.
        prop_one

   ;    % This specifies an additonal check on the argument value, apart from the fact that it can be parsed to be
        % of the right type. The checker takes the parsed value and checks it. When the value isn't accepted, a
        % command_line_error exception is thrown.
        prop_check(checker)

   ;    % This specifies that the value of an environment variable can be used to set the argument and its value.
        % The argument of prop_env is the name of the environment variable. It will be used only when there are no
        % command line arguments for this argument.
        prop_env(string)

   ;    % Description of the command line argument for the automatic usage info generation. This string shouldn't
        % be broken into lines by newline characters, since this is done automatically. Newlines are supported,
        % however, and will be included in the reshaped text.
        prop_description(string)
   .


%--------------------------------------------------------------------------------------------------------------
% 3. Processing the command line arguments

% Parse the command line according to the supplied argument descriptions. The command line parameters, and the
% environment are determined by io.command_line_arguments/3 and io.environment.get_environment_var_map/3,
% respectively. Therefore it is an IO action. The parse results (the arguments and non-arguments) are returned.

:- pred colipa(
    % The argument descriptions. Those are pairs of argument identifiers and property lists.
    assoc_list(cla, list(property))::in,

    % The command line arguments parse result.
    arguments::out,

    % The non-argument command line parameters.
    list(string)::out,

    io::di, io::uo)
is det.



% Parse the command line according to the supplied argument descriptions. The command line parameters, and the
% environment are passed in the second and third arguments. Therefore this doesn't need to be an IO action. The
% parse result (the arguments and non-arguments) is returned.

:- pred colipa1(
    % The argument descriptions. Those are pairs of argument identifiers and property lists.
    assoc_list(cla, list(property))::in,

    % The command line parameters
    list(string)::in,

    % The environment variables
    io.environment_var_map::in,

    % The command line arguments parse result.
    arguments::out,

    % The non-argument command line parameters.
    list(string)::out)
is det.



%----------------------------------------------------------------------------------------------------
% 4. Argument query functions
%
% Theses functions are used to get values out of command line arguments.


% The parsed and assembled command line parameters. This contains all the argument/non-argument values and argument
% descriptions.

:- type arguments.


% This typeclass consists all types for which an argument value parser has been defined. If you want to be able to
% use other types on the command line, then instantiate this class. The types need to define the from_str/1
% function, which returns either the successfully parsed value in fs_ok(Val) or a parse error in fs_error(Msg),
% where Msg is an error message. The message will then be included in a cle_malformed_value exception.

:- typeclass argument(T) where [
       func from_str(string) = from_str_result(T)
   ].

:- type from_str_result(T)
    ---> fs_ok(T)
    ;    fs_error(string).


% Predefined argument value types.
:- instance argument(string).
:- instance argument(int).
:- instance argument(float).


% The compiled description of one command line argument from the argument properties
:- type argdesc.


% Generate a name for the argument suitable for users. This is a concatenation of the short and long argument
% names. All such names are included. For instance, "-n/-b/--num".
:- func argname(argdesc) = string.


% Generate an enumeration of argument names for a list of command line arguments. Each argument is named according
% to argname/1, separating them by ", " and (last one) by " and ". For instance, "-f/--foo, --bar and -b/--baz".
:- func argsname(list(argdesc)) = string.


% Query the value of a requiered argument. The value can come from either the command line, or from an environment
% variable (if any, see prop_env) or from a default value (see prop_default). The first one of these three, which
% is present, specifies the value(s). The other ones are ignored.
%
% This doesn't work for arguments which don't take a value. For those, an ade_takes_no_value/2 exception
% is thrown.

:- func arg_value(arguments, cla) = T <= argument(T).


% Query the value of am optional argument. The value can come from either the command line, or from an environment
% variable (see prop_env), if any. There can't be specified a default value (see prop_default). For such arguments,
% you need to use arg_value/2 or arg_values/2.
%
% This doesn't work for arguments which don't take a value. For those, an ade_takes_no_value/2 exception
% is thrown.

:- func arg_maybe_value(arguments, cla) = maybe(T) <= argument(T).


% Query the list of values given for an argument. The argument can take an arbitrary number of values. When there
% aren't any, but an environment variable has been defined, then the value of the environment variable is used (you
% get a one-element list). When there both the command line argument has no occurences on the command line and no
% default has been defined, you can still get a default value, if one has been set with prop_default.

:- func arg_values(arguments, cla) = list(T) <= argument(T).


% Query an argument that is a switch. That is, an argument which doesn't take a value and can occur zero or one
% times. The result is whether the argument was given on the command line.

:- func arg_switch(arguments, cla) = bool.


% Query the number of times an argument has been given on the command line. There are no limitations to the
% argument's properties. arg_times/2 can be used with any argument.

:- func arg_times(arguments, cla) = int.


%----------------------------------------------------------------------------------------------------
% 5. Error handling

% The origin of a command line argument value. This is included in some errors and is used for error message
% generation.

:- type origin
    ---> origin_command_line    % The value is from the command line.
    ;    origin_default         % The value comes from the default for its argument.
    ;    origin_env             % The value comes from an environment variable.
    ;    origin_none.           % No origin yet. This occurs only internally to colipa. It doesn't occur in
                                % command_line_errors and arg_desc_errors.


% An error on the command line. The command line paramters don't fit the expectations of the argument descriptions.

:- type command_line_error

    ---> % An unknown short command line argument. Starts with "-".
         cle_unknown_short_arg(
             char,      % short argument
             int,       % number of argument in the command line
             string)    % the parameter

    ;    % An unknown long command line argument. Starts with "--".
         cle_unknown_long_arg(
             string,    % long argument
             int,       % number of argument in the command line
             string)    % the parameter

    ;    % An ambigious abbreviation of a long command line argument.
         cle_ambigious_abbrev(
             string,            % uncomplete long argument, given on the command line
             list(string),      % list of long arguments which could be meant
             int,               % number of argument in the command line
             string)            % the parameter

    ;    % Missing value for an argument that takes a value. This can happen only at the end of the command line.
         cle_missing_value(
             argdesc,                % The argument description of the argument with missing value
             int,                    % number of the last parameter
             string)                 % the last parameter

    ;    % There's a value for an argument, which doesn't take a value. This can only happen, when the argument
         % consist of only one parameter.
         cle_unexpected_value(
             string,    % The value
             int,       % number of argument in the command line
             string)    % the parameter

    ;    % Parse error in the value of a command line argument or the default.
         cle_malformed_value(
             argdesc,           % The argument's description
             origin,            % Where the malformed string comes from
             string,            % Invalid argument value
             string)            % Parse error message

    ;    % An argument value could be parsed, but it isn't inside the valid range. This has been determined by a
         % checker.
         some [T] cle_invalid_value(
             argdesc,           % Argument description
             T,                 % Invalid argument value
             origin,            % Origin of the invalid value
             maybe(string))     % Checker error message

    ;    % The number of occurrences of the argument is too low.
         cle_too_few_occurrences(
             argdesc,           % Argument description
             int,               % Minimum allowed number of occurrences
             int)               % Found number of occurrences

    ;    % The number of occurrences of the argument is too large.
         cle_too_many_occurrences(
             argdesc,           % Argument description
             int,               % Maximum allowed number of occurrences
             int)               % Found number of occurrences

         % There has been no subcommand found, but it is required. That's for programs which make use of the
         % program predicate.
    ;    cle_no_subcommand(
             string)            % Error message

         % The args_one/3 restriction isn't met. See the "Placing additional constraints on command line arguments"
         % section.
    ;    cle_args_one(
             arguments,         % Compiled arguments
             list(cla),         % List of arguments that are restricted
             string)            % Error message

         % The args_at_least_one/3 restriction isn't met. See the "Placing additional constraints on command line
         % arguments" section.
    ;    cle_args_at_least_one(
             arguments,         % Compiled arguments
             list(cla),         % List of arguments that are restricted
             string)            % Error message

         % The args_at_most_one/3 restriction isn't met. See the "Placing additional constraints on command line
         % arguments" section.
    ;    cle_args_at_most_one(
             arguments,         % Compiled arguments
             list(cla),         % List of arguments that are restricted
             string)            % Error message

         % The user has chosen an unknown subcommand.
    ;    cle_unknown_subcommand(
             string,            % The subcommand chosen by the user.
             list(string))      % The list of all subcommands

    ;    cle_ambigious_subcommand(
             string,            % The prefix/abbreviation as specified by the user
             list(string),      % The list of possible completions
             list(string)       % The list of all subcommands
         )

    .



% An error in the specification of a command line argument. This probably means that there's a bug.

:- type arg_desc_error

    ---> % An argument is defined twice or more times. This means the same identity has been used for several
         % different arguments.
         ade_duplicate_desc(
             cla,               % The non-unique argument identifier
             list(argdesc))     % The arguments with that identifier (two or more)

    ;    % No long or short argument names have beens specified for an argument.
         ade_no_name(
             cla)           % Argument identifier

    ;    % An argument is missing in the arguments descripton assoc_list.
         ade_unknown_arg(
             cla)          % The unknown argument

    ;    % Two or more properties in the property list of an argument conflict with each other.
         ade_conflict(
             cla,               % The argument
             list(property))    % List of contradicting properties

    ;    % The type of the default value of an argument doesn't match the type of the value, which is queried by
         % an argument value query function. Use type_desc.type_name/1 to get the names of the two types.
         ade_type_mismatch_default(
             argdesc,                   % The argument description
             string,                    % Name of the argument value query function
             type_desc.type_desc,       % The type of the default
             type_desc.type_desc)       % The return type of the arg_value/2 function

    ;    % The type of a checker doesn't match the type of the value, which is queried by an argument query
         % function. Use type_desc.type_name/1 to get the names of the two types.
         ade_type_mismatch_checker(
             argdesc,                   % The argument description
             string,                    % Name of the argument value query function
             origin,                    % Where the value comes from
             type_desc.type_desc,       % The value type of the checker predicate
             type_desc.type_desc)       % The return type of the argument query function

    ;    % The allowed minimum number of occurences of the argument, as specified in the argument properties,
         % doesn't match the function which the argument's value is queried with. For the arg_value/2 function, the
         % allowed minimum number occurences must be 1. For arg_switch/1, the minimum must be 0 and the maximum
         % must be 1.
         ade_min_number_of_occurrences_mismatch(
             argdesc,       % The argument description
             int,           % Minimum number of occurences which must be allowed according to the arg_... function.
             string)        % Name of the argument query function

    ;    % The allowed maximum number of occurences of the argument, as specified in the argument properties,
         % doesn't match the function which the argument's value is queried with. For the arg_value/2 function, the
         % allowed maximum number occurences must be 1. For arg_switch/1, the minimum must be 0, the maximum must
         % be 1.
         ade_max_number_of_occurrences_mismatch(
             argdesc,           % The argument description
             int,               % Maximum number of occurences which must be allowed according to the argument
                                % query function.
             string)            % Name of the argument query function

    ;    % An argument that doesn't take a value has been queried with arg_value/2, arg_maybe_value/2 or
         % arg_values/2. Or an argument that takes a value has been queried with arg_switch.
         ade_takes_value_mismatch(
             argdesc,           % The argument description description
             bool,              % If the argument takes a value
             bool,              % If the query function expects an argument that takes a value
             string)            % Name of the argument query function

    ;    % An argument, which has a default value, is queried in a way which doesn't allow a default. This means
         % that it is queried with the arg_maybe_value/2 function.
         ade_has_default_mismatch(
             argdesc,           % The argument description
             bool,              % If the argument has a default value
             bool,              % If the query function expects an argument that has a default value
             string)            % Name of the argument query function

    ;    % Several arguments with the same long name are defined.
         ade_multiple_long(
             string,            % The long argument name
             list(argdesc))     % The list of the descriptions of the long argument name in question

    ;    % Several arguments with the same short name are defined.
         ade_multiple_short(
             char,              % The short argument name
             list(argdesc))     % The list of the descriptions of the long argument name in question

    ;    % An argument, that isn't a switch has been queried with arg_switch. This means, the argument hasn't been
         % declared to be a switch with prop_switch or prop_inverse_switch.
         ade_not_a_switch(
             cla,               % The argument
             string)            % Name of the argument query function

    ;    % The argument has a value, but no name for the value has been specified. This happens when it has been
         % determined that the argument has a value, by using prop_default. But there is no prop_value to name that
         % value.
         ade_missing_valname(
             cla,               % The argument identity
             list(property))    % The properties, which designate that the argument has a value. This can only be
                                % one prop_default/2, so far.
    .


% Generate an expressive error message for a command line error.
:- func cle_message(command_line_error) = string.


% Generate an expressive error message for an argument description error.
:- func ade_message(arg_desc_error) = string.


% Convert any type to string, with "argdesc" and "arguments" components abbreviated to "<<argdesc>>" or
% "<<arguments>>" respectively. This is meant to provide more readable output, by hiding parts that aren't of
% interest.
:- pred shortened(T::in, string::out) is det.
:- func shortened(T) = string.


%----------------------------------------------------------------------------------------------------
% 6. Argument value checkers

% This does an additonal check on a argument value, apart from the fact that it can be parsed to be of the right
% type. The checker takes the parsed value and checks it. When the value isn't accepted, it throws an exception.

:- type checker.


% Build a checker from a predicate. The predicate must take a value with the type of the values of the
% corresponding command line argument. When it's invalid, an error message is to be returned as "yes(Message)".
% When it's valid, "no" is to be returned. When the checker created by make_checker is applied, it will throw a
% cle_invalid_value exception in case the value is invalid.

:- func make_checker(  pred(T, maybe(string))::pred(in, out) is det  )
        = (checker::out) is det  <=  argument(T).


% A checker for integer values, which must be zero or greater than zero.

:- func nonneg_integer = checker.


% A checker for integer values, which must be greater than zero.

:- func positive_integer = checker.




%----------------------------------------------------------------------------------------------------
% 7. Automatic usage info generation from the argument descriptions


% Print the usage information for all arguments in the argument description list. This determines if the standard
% output goes to a terminal. If this is the case, the terminal's width is used for the lines, and color is
% generated for the output. If it isn't, then 80 is used as the width, and no color is generated.

:- pred print_usage_info(
    assoc_list(cla, list(property))::in,   % The command line descriptions
    io::di, io::uo)
is det.


% Print the usage information for some of the arguments in the argument description list. This determines if the
% standard output goes to a terminal. If this is the case, the terminal's width is used for the lines, and color is
% generated for the output. If it isn't, then 80 is used as the width, and no color is generated.

:- pred print_usage_info(
    assoc_list(cla, list(property))::in,   % The command line descriptions
    list(cla)::in,                         % List of the identifiers of the arguments to print
    io::di, io::uo)
is det.


% For a program that takes a subcommand, print the usage info with subcommands. First a list of the subcommands is
% printed, preceded by "The subcommands are:". Then the command line arguments are printed, preceded by "The
% arguments are:". This doesn't print a header.
%
% It is determined if the standard output goes to a terminal. If this is the case, the terminal's width is used for
% the lines, and color is generated for the output. If it isn't, then 80 is used as the width, and no color is
% generated.

:- pred print_usage_info_with_subcommands(
    assoc_list(string, string)::in,        % The subcommands with documentation
    assoc_list(cla, list(property))::in,   % The command line descriptions
    io::di, io::uo)
is det.



% For a program that takes a subcommand, print the usage info with subcommands. First a list of the subcommands is
% printed, preceded by "The subcommands are:". Then the command line arguments are printed, preceded by "The
% arguments are:". This doesn't print a header.
%
% The usage information for only some the arguments in the argument description list is printed. The third argument
% determines which.
%
% It is determined if the standard output goes to a terminal. If this is the case, the terminal's width is used for
% the lines, and color is generated for the output. If it isn't, then 80 is used as the width, and no color is
% generated.

:- pred print_usage_info_with_subcommands(
    assoc_list(string, string)::in,        % The subcommands with documentation
    assoc_list(cla, list(property))::in,   % The command line descriptions
    list(cla)::in,                         % List of the identifiers of the arguments to print. This is a subset of
                                           % the list in the second argument.
    io::di, io::uo)
is det.


% An extended usage info printing predicate. This is used by program/6. The two assoc_lists can be shorter than the
% ones passed to the program/6 call. This is useful for when you don't want to print all actions or all
% subcommands.

:- pred print_extended_usage_info(
    (func(string) = string)::in,           % Function that takes the program name, as determined by
                                           % io.progname_base and returns the header for the usage info. This is
                                           % used by the "help" subcommand.
    assoc_list(string, string)::in,        % The subcommands with documentation
    assoc_list(cla, list(property))::in,   % The command line descriptions
                                           % the list in the second argument.
    io::di, io::uo
) is det.


% Generate the usage information for all the arguments in the argument descriptions list. See the prop_description
% property and get_argdescs, below.

:- pred usage_info(
    list(argdesc)::in,          % The argument descriptions
    int::in,                    % The maximum line width. This should be the width of the terminal on which the
                                % usage info is to be displayed.
    int::in,                    % The maximum width of long argument column
    int::in,                    % The maximum width of short argument column
    bool::in,                   % If to use color
    list(string)::out)          % List of the pieces of usage information for each argument.
is det.


% Generate the usage information for one argument, which is specified by the argument description.

:- pred usage_info_one(
    int::in,            % Width of the terminal
    int::in,            % Maximum width of the column with the long arguments
    int::in,            % Maximum width of the column with the short arguments
    bool::in,           % If to use color
    argdesc::in,        % Description for the argument
    string::out)        % Usage info for the one argument
is det.


% If your program makes use of subcommands, then this predicate will help to make a list consisting of the
% subcommands and descriptions. The subcommands will be desplayed in green color, if stdout is connected to a
% terminal.

:- pred print_subcommands_info(
    assoc_list(string, string)::in,    % List of the subcommands and their descriptions.
    io::di, io::uo)
is det.


% Determine the width of the terminal in columns. If the file descriptor isn't connected to a terminal, use the
% default width.

:- pred terminal_width_fd(
    int::in,            % File descriptor for the terminal
    int::in,            % Default width, for non-terminals
    int::out,           % Width of the terminal in columns
    io::di, io::uo)
is det.


% Determine the width of a terminal in columns, which is encapsulated inside an text_output_stream. If the file
% descriptor isn't connected to a terminal, use the default value.

:- pred terminal_width(
    io.text_output_stream::in,  % File descriptor for the terminal
    int::in,                    % Default width, for non-terminals
    int::out,                   % Width of the terminal in columns
    io::di, io::uo)
is det.


% Determine if the specified file descriptor is connected to a terminal.

:- pred isatty(
    int::in,            % File descriptor for the terminal
    bool::out,          % Whether it is a terminal
    io::di, io::uo)
is det.


% Get a list of argument descriptions from the compiled arguments.

:- pred get_argdescs(
    arguments::in,         % The compiled arguments
    list(cla)::in,         % List of the identifiers for the arguments to retrieve
    list(argdesc)::out)    % List of the argument descriptions belonging to the identifiers
is det.


% Break up continuous text into lines, such that the maximum line width is preserved. To the left of the lines, a
% column consisting of spaces is created (when the start column isn't zero). This column is left out from the
% first line, So the caller can position something there. The text should probably not be formatted into lines.
% Leave the breaking into lines to layout_text.
%
% But newlines inside are respected and result in new lines in the output. Lines (as separated by newlines) are
% also allowed to begin with an indentation, which consists of spaces. This indentation is respected as well. This
% means that it occurs in the output, after the start column. It is applied to all lines that the input line in
% question is broken into.

:- pred layout_text(
    int::in,            % Start column. This makes a column of spaces at the left of the text.
    int::in,            % Width of the second column, which will contain the text.
    string::in,         % The text which is to be layed out
    string::out)        % The layed out text.
is det.


% Print a text to stdout_stream. The width of the terminal is determined and used as the maximum line length for
% the text. The text will be wrapped at the end of the line, such that no words are split in two (except for very
% long words). If stdout_stream isn't connected to a terminal, 80 will be used as the line width.
%
% This is a frontend to layout_text/4. The same formatting rules apply.
%
% "print_on_terminal(Str, !IO)" is an abbreviation for "print_on_terminal(stdout_stream, Str, !IO)".

:- pred print_on_terminal(
    string::in,                 % Text to print
    io::di, io::uo)
is det.


% Print a text to a stream, which can be connected to a terminal. If the stream is connected to a terminal, the
% width is determined and used as the maximum line length for the text. The text will be wrapped at the end of the
% line, such that no words are split in two (except for very long words). If the stream isn't connected to a
% terminal, 80 will be used as the line width.
%
% This is a frontend to layout_text/4. The same formatting rules apply.

:- pred print_on_terminal(
    io.text_output_stream::in,  % Where to print to
    string::in,                 % Text to print
    io::di, io::uo)
is det.


% Sort an argument description list by the first long argument name. This is stable, the arguments with no long
% name will stay in the same order. This isn't a Unicode-aware sorting, it's just based on builtin.compare/3.
% Making it Unicode-aware would add a dependency on a library (such as glib). It isn't worth that, since it would
% hardly ever be needed.

:- pred sort_argdescl(
    assoc_list(cla, list(property))::in,
    assoc_list(cla, list(property))::out)
is det.



%----------------------------------------------------------------------------------------------------
% 8. Placing additional constraints on command line arguments
%
% While argument descriptions can place constraints and checks on individual arguments, they can't place
% restrictions on groups of arguments. For this purpose, the following predicates are implemented. They each check
% something about several arguments and throw a command_line_error if the constraint isn't observed.
%
% The message that is generated by the cle_message/1 function for such a command line error, can be customized by
% the function that is passed to the corresponding predicate. When this is "yes(Func)", then function is called,
% passing it the list of the names of the arguments involved. There's a helper function called argsname/1, which
% names the arguments, separating them by commas and the wort "and" at the end. This uses the argname/1 function
% for each argument. When the third argument is "no", then a default error message is generated.


% Exactly one of the arguments must be specified on the command line. When the same argument is allowed to occur
% several times on the command line, then doesn't mean that args_one will throw an exception. Only the number of
% *different* arguments is restricted.
:- pred args_one(
    arguments::in,                              % The compiled arguments
    list(cla)::in,                              % List of the argument that must observe the constraint.
    maybe(func(list(argdesc)) = string)::in     % Optional command line error message generator
) is det.


% The default error message generator of the args_one/3 predicate.
:- func args_one_defmsg(list(argdesc)) = string.


% At least one of the arguments must be specified on the command line. When the same argument is allowed to occur
% several times on the command line, then doesn't mean that args_one will throw an exception. Only the number of
% *different* arguments is restricted.
:- pred args_at_least_one(
    arguments::in,                              % The compiled arguments
    list(cla)::in,                              % List of the argument that must observe the constraint.
    maybe(func(list(argdesc)) = string)::in     % Optional command line error message generator
) is det.


% The default error message generator of the args_at_least_one/3 predicate.
:- func args_at_least_one_defmsg(list(argdesc)) = string.


% At least one of the arguments must be specified on the command line. When the same argument is allowed to occur
% several times on the command line, then doesn't mean that args_one will throw an exception. Only the number of
% *different* arguments is restricted.
:- pred args_at_most_one(
    arguments::in,                              % The compiled arguments
    list(cla)::in,                              % List of the argument that must observe the constraint.
    maybe(func(list(argdesc)) = string)::in     % Optional command line error message generator
) is det.


% The default error message generator of the args_at_most_one/3 predicate.
:- func args_at_most_one_defmsg(list(argdesc)) = string.




%----------------------------------------------------------------------------------------------------
% 9. Framework for a program with subcommands

% This predicate manages a program with subcommands. It strives to automate as much of its command line processing
% as possible. The subcommand is the first non-argument command line parameter. It's passed to the predicate as the
% first argument. It is removed from the list of command line arguments. Command line arguments can occur on both
% sides of the subcommand.
%
% If the subcommand is "help", the usage info is printed. If the specified subcommand is unknown, an error message
% is printed and program/6 finishes. If no subcommand is specified, a short message is printed, which tells the
% user to call the program with "help". The command line parsing is done inside program/6, so user doesn't have to
% call colipa/5 or colipa1/5.

:- pred program(
    assoc_list(string, string)::in,        % The subcommands with documentation. This should *not* include the
                                           % "help" subcommand.
    assoc_list(cla, list(property))::in,   % The command line descriptions
    (func(string) = string)::in,           % Function that makes a header to print for "help" subcommand from the
                                           % program name. See print_usage_info_with_subcommands/4.
    (pred(string,                          % The chosen subcommand
          arguments,                       % The compiled command line arguments
          list(string),                    % The non-arguments, exluding the subcommand
          io, io)
     :: in(pred(in, in, in, di, uo) is cc_multi)),

    io::di, io::uo
) is cc_multi.


% This is the same as program/6, except for more flexible usage info generation. The usage info is printed by an IO
% action (third argument). print_extended_usage_info/5 can be used for this. This way, you can print a usage info
% that doesn't include all subcommands or not all command line arguments.

:- pred program_extended(
    assoc_list(string, string)::in,        % The subcommands with documentation. This should *not* include the
                                           % "help" subcommand.
    assoc_list(cla, list(property))::in,   % The command line descriptions

    (pred(io, io)                          % Usage info printing predicate
     :: in(pred(di, uo) is det)),

    (pred(string,                          % The chosen subcommand
          arguments,                       % The compiled command line arguments
          list(string),                    % The non-arguments, exluding the subcommand
          io, io)
     :: in(pred(in, in, in, di, uo) is cc_multi)),

    io::di, io::uo
) is cc_multi.



% Type for an exception that is meant to be caught by main. It means that the program should exit with the provided
% exit code. There should be a handler like this, at the top level of the program:
%
% (
%     try [io(!IO)]
%         ...
%     then true
%     catch (shutdown(ExitCode)) ->
%         io.set_exit_status(ExitCode, !IO).
% )
%
% This is used by catch_cle_ade/5.

:- type shutdown ---> shutdown(int).


% This catches command_line_error's and arg_desc_error's, which are throw by the specified predicate. An error
% message is printed and catch_cle_ade succeeds.

:- pred catch_cle_ade(
    int::in,                                    % Exit code for command line error
    int::in,                                    % Exit code for argument description error
    pred(io, io)::in(pred(di, uo) is cc_multi), % Predicate to wrap
    io::di, io::uo
) is cc_multi.


% This is like catch_cle_ade/4, but will also print the Mercury form (the data constructor) of the exception. This
% can be useful for debugging.

:- pred catch_cle_ade_extra(
    int::in,                                    % Exit code for command line error
    int::in,                                    % Exit code for argument description error
    pred(io, io)::in(pred(di, uo) is cc_multi), % Predicate to wrap
    io::di, io::uo
) is cc_multi.



% % For testing
% :- pred main(io::di, io::uo) is det.


%==============================================================================================================

:- implementation.
:- import_module int, pair, exception, map, solutions, io.environment, require, deconstruct, type_desc, univ.
:- import_module stringsupp, unicode.


%----------------------------------------------------------------------------------------------------
% Command line argument properties


% Compile a list of argument properties to an argdesc value.
:- pred condense_properties(
    cla::in,               % The argument identifier, whose properties are condensed
    list(property)::in,    % List of the properties of the argument
    argdesc::out           % Result: condensed argument description
).

condense_properties(Arg, L, ArgDesc) :-

    argdesc_list(L, func(prop_long(Long1)) = Long1 is semidet, _, LongL),
    argdesc_list(L, func(prop_short(Short1)) = Short1 is semidet, _, ShortL),

    argdesc_maybe_one(Arg, L, extract_valname, _, MaybeMaybeValname),
    MaybeValname = maybe.maybe_default(no, MaybeMaybeValname),

    argdesc_maybe_one(Arg, L,
        func(prop_default(Default)) = 'new default_wrapper'(Default) is semidet,
        DecidingDefault, MaybeDefault),

    argdesc_maybe_one(Arg, L, extract_min, PropsMin, MaybeMin),
    argdesc_maybe_one(Arg, L, extract_max, PropsMax, MaybeMax),
    argdesc_maybe_one(Arg, L, func(prop_check(Checker)) = Checker is semidet, _, MaybeChecker),
    argdesc_maybe_one(Arg, L, func(prop_env(Env)) = Env is semidet, _, MaybeEnv),
    argdesc_maybe_one(Arg, L, func(prop_description(Description)) = Description is semidet, _, MaybeDescription),
    argdesc_maybe_one(Arg, L, extract_inverse, _, MaybeInverse),

    % Make a value for the argdesc_min/argdesc_max components of the argdesc type, from the DecidingProps and
    % MaybeVal output arguments of the argdesc_maybe_one predicate.
    MinmaxArrange =
        (func(Props, MaybeMinmax) = Val :-
            (   MaybeMinmax = no,
                Val = no
            ;   MaybeMinmax = yes(T),
                Val = yes(T - map(fst, Props)
            ))),

    ArgDesc =
        argdesc(
            Arg,
            LongL,
            ShortL,
            MaybeValname,
            MaybeDefault,
            MinmaxArrange(PropsMin, MaybeMin),
            MinmaxArrange(PropsMax, MaybeMax),
            MaybeChecker,
            MaybeEnv,
            MaybeDescription,
            MaybeInverse),

    % No argument name given.
    ( if   LongL  = [],
           ShortL = []
      then throw(ade_no_name(Arg))
      else true
    ),

    % When the argument has a default, then it must take and name a value.
    ( if   MaybeDefault = yes(_),
           MaybeValname = no
      then throw(ade_missing_valname(Arg, map(fst, DecidingDefault)))
      else true
    ).



% Take a list of properties and extract certain values from it. A selector/extractor function is used. It can
% succeed or fail. It returns the value which a property of a certain kind defines. The succeeding properties and
% values are collected and returned in the Props and Vals arguments. Both of them are sorted and duplicates
% removed.

:- pred argdesc_list(
    % List of all the properties of an argument description.
    list(property)::in,

    % Extractor function.
    (func(property) = T)::in(func(in) = out is semidet),

    % List of the properties, together with the values which they deliver. This list is sorted.
    list(pair(property, T))::out,

    % Just the values. This list is sorted.
    list(T)::out
) is det.

argdesc_list(Ps, Func, DecidingProps, Vals) :-
    argdesc_list_nosort(Ps, Func, DecidingProps0, Vals0),
    list.sort_and_remove_dups(DecidingProps0, DecidingProps),
    list.sort_and_remove_dups(Vals0, Vals).


% Same as argdesc_list, but don't sort the two result lists. This is for prop_check and prop_default, which take
% values that can't be sorted. In this case, two values of prop_check or prop_default, are always treated as
% different.

:- pred argdesc_list_nosort(
    % List of all the properties of the argument description.
    list(property)::in,

    % Extractor function.
    (func(property) = T)::in(func(in) = out is semidet),

    % List of the properties, together with the values which they deliver.
    list(pair(property, T))::out,

    % Just the values.
    list(T)::out
) is det.

argdesc_list_nosort(Ps, Func, DecidingProps, Vals) :-
    foldl2(
        (pred(Prop::in, Props1::in, Props2::out, Vals1::in, Vals2::out) is det :-
            (
                if   Func(Prop) = Val
                then Vals2 = Vals1 ++ [Val],
                     Props2 = Props1 ++ [Prop - Val]
                else Vals2 = Vals1,
                     Props2 = Props1
            )),
        Ps,
        [],
        DecidingProps,
        [],
        Vals).


% For a property (which has a value) that must occur zero or one times, extract its value. A selector/extractor
% function is used. It can succeed or fail, but it must succeed at most once. When it succeeds more than once, an
% exception is thrown. The result is either "no" (no success) or "yes(Val)" (one success), where Val is the result
% of the extractor function.

:- pred argdesc_maybe_one(
    % The identity of the argument description
    cla::in,

    % List of all the properties of the argument description.
    list(property)::in,

    % Extractor function.
    (func(property) = T)::in(func(in) = out is semidet),

    % The properties that lead to the one, unique result value. This is at least one, but it can also be several
    % ones. The latter can happen, when several properties specify the same value (as determined by the extractor
    % function).
    list(pair(property, T))::out,

    % The one, unique result value, if any.
    maybe(T)::out
) is det.

argdesc_maybe_one(Arg, PropL, Func, DecidingProps, MaybeVal) :-
    argdesc_list(PropL, Func, Props, Vals),
    (
        Vals = [],
        MaybeVal = no,
        DecidingProps = []
    ;
        Vals = [Val],
        MaybeVal = yes(Val),
        DecidingProps = Props
    ;
        Vals = [_,_|_],
        throw(ade_conflict(Arg, list.map(fst, Props)))
    ).


% Succeed for properties that determine a value name. This is for properties that specify a value name, as well as
% properties that specify that no value name is allowed.

:- func extract_valname(property) = maybe(string) is semidet.

extract_valname(prop_value(ValName))        = yes(ValName).
extract_valname(prop_switch_value(ValName)) = yes(ValName).
extract_valname(prop_switch)                = no.
extract_valname(prop_inverse_switch)        = no.



% Succeed for properties that determine the minumum number of occurencs of an argument.

:- func extract_min(property) = int is semidet.

extract_min(prop_switch_value(_)) = 0.
extract_min(prop_min_times(Min))  = Min.
extract_min(prop_switch)          = 0.
extract_min(prop_inverse_switch)  = 0.
extract_min(prop_one)             = 1.


% Succeed for properties that determine the maximum number of occurencs of an argument.

:- func extract_max(property) = int is semidet.

extract_max(prop_switch_value(_)) = 1.
extract_max(prop_max_times(Max))  = Max.
extract_max(prop_switch)          = 1.
extract_max(prop_inverse_switch)  = 1.
extract_max(prop_one)             = 1.



% Succeed for properties that determine if the switch is inverse.

:- func extract_inverse(property) = bool is semidet.

extract_inverse(prop_switch) = no.
extract_inverse(prop_inverse_switch) = yes.


%----------------------------------------------------------------------------------------------------
% Argument descriptions


:- type default_wrapper
    ---> some[T] default_wrapper(T).


% The description of an argument.
:- type argdesc
    ---> argdesc(
        % The identifier of the argument
        argdesc_cla :: cla,

        % Long names of the argument
        argdesc_long :: list(string),

        % Short names of the argument
        argdesc_short :: list(char),

        % If the argument takes a value, and if so, the name of that value. The name is specified with pred_value
        % and is used for error message generation.
        argdesc_valname :: maybe(string),

        % Default value
        argdesc_default :: maybe(default_wrapper),

        % Minimum number of occurrences of the argument, together with the list of properties that have determined
        % that number. It defaults to 0. The minimum number can be no larger than 1, when prop_env or prop_default
        % are used. Else an arg_desc_error is thrown. The properties list is needed for the generation of the
        % ade_min_number_of_occurrences_mismatch exception, inside the argument query functions. It is thrown in
        % case the minimum doesn't match the minimum, which expected by the argument query function.
        argdesc_min :: maybe(pair(int, list(property))),

        % Maximum number of occurrences of the argument, together with the list of properties that have determined
        % that number. The properties list is needed for the generation of the
        % ade_max_number_of_occurrences_mismatch exception, inside the argument query functions. It is thrown in
        % case the maximum doesn't match the maximum, which expected by the argument query function.
        argdesc_max :: maybe(pair(int, list(property))),

        % Argument value checker. We can't compare checkers, because they are predicates. Therefore prop_checker
        % must handle conflicting checkers differently.
        argdesc_checker :: maybe(checker),

        % Environment variable to serve as a default, if an argument hasn't been supplied.
        argdesc_env :: maybe(string),

        % Description for the argument. Used for automatic help text generation.
        argdesc_description :: maybe(string),

        % For switches: if it's a inverse switch
        argdesc_inverse :: maybe(bool)
    ).



% Generate a user friendly name for an argument.
argname(ArgDesc) =
    string.join_list("/",
        map(func(Char) = "-" ++ char_to_string(Char), ArgDesc ^ argdesc_short)
        ++
        map(func(Str) = "--" ++ Str, ArgDesc ^ argdesc_long)).


% Generate user friendly enumeration of argument names.
argsname([]) = "".

argsname([A]) = argname(A).

argsname(L@[_, _|_]) = Name :-
    (
        if   split_last(L, Begin0, End0)
        then Begin = Begin0,
             End = End0
        else unexpected($pred, "impossible.")
    ),
    Name = string.join_list(", ", map(argname, Begin)) ++ " and " ++ argname(End).



%----------------------------------------------------------------------------------------------------
% Argument maps (storage space for the argument descriptions).


% Maps from things to the argument descriptions. Things are the argument identifier, short argument and long
% argument names. For the long argument names, ALL UNIQUE ABBREVIATIONS THE ARGUMENT NAME ARE ALSO INCLUDED.
:- type argmaps
    ---> argmaps(
        argmaps_cla     :: map(cla, argdesc),
        argmaps_long    :: map(string, argdesc),
        argmaps_short   :: map(char, argdesc)
    ).


% Look up an argument description in the short argument map (argdesc_short). The Num and Par arguments are
% for error reporting.
:- pred lookup_argdesc_short(
    arguments::in,
    char::in,
    int::in,
    string::in,
    argdesc::out)
is det.

lookup_argdesc_short(Arguments, Short, Num, Par, Argdesc) :-
    (
        if   map.search(Arguments ^ arguments_argmaps ^ argmaps_short, Short, Argdesc0)
        then Argdesc = Argdesc0
        else throw(cle_unknown_short_arg(Short, Num, Par) : command_line_error)
    ).



% Look up an argument description in the long argument map (argdesc_long). The Num and Par arguments are
% for error reporting.
:- pred lookup_argdesc_long(
    arguments::in,      % The compiled argument descriptions
    string::in,         % The long argument name to lookup
    int::in,            % In case of unknown argument, the number of the command line parameter which is being
                        % processed. Used for exception.
    string::in,         % In case of unknown argument, the entire command line paramter, which the long argument is
                        % in. Used for exception.
    argdesc::out)       % When the argument is known, its argument description. Throws an exception otherwise.
is det.

lookup_argdesc_long(Arguments, Long, Num, Par, Argdesc) :-
    (
        if   map.search(Arguments ^ arguments_argmaps ^ argmaps_long, Long, Argdesc0)
        then Argdesc = Argdesc0
        else
            % Argument not found. Determine if it is an ambigious abbreviation.
            get_argdescs(Arguments, Arguments ^ arguments_args, ArgDescs),
            map(pred(ArgDesc::in, Longs::out) is det :- Longs = ArgDesc ^ argdesc_long,
                ArgDescs,
                LongLL),
            LongL = list.condense(LongLL),
            prefixof(
                Long,
                LongL,
                AbbreviatedL),  % List of the long arguments are abbreviated by Long.
            (
                if length(AbbreviatedL) > 1
                then throw(cle_ambigious_abbrev(Long, AbbreviatedL, Num, Par) : command_line_error)
                else throw(cle_unknown_long_arg(Long, Num, Par) : command_line_error)
            )
    ).



% For which long arguments is CLP an abbreviation?

:- pred prefixof(
    string::in,             % The long argument name (or an abbreviation) from the command line
    list(string)::in,       % All the long argument names (of all arguments)
    list(string)::out       % The argument names which would be abbreviated by the long argument name, if it
                            % weren't ambigious
) is det.

prefixof(CLP, Longs, AbbreviatedL) :-
    solutions((
        pred(Long::out) is nondet :-
            list.member(Long, Longs),
            string.append(CLP, _, Long)),
        AbbreviatedL).



% Assemble the argmaps from the list of argument descriptions.

:- pred compile_argmaps(
    list(argdesc)::in,     % The argument descriptions (created out of the argument-property-lists)
    argmaps::out)          % The argmaps.
is det.

compile_argmaps(Unsorted, Argmaps) :-

    compile_one_argmap(
        Unsorted,
        func(ArgDesc) = [ArgDesc ^ argdesc_cla],
        func(Arg, ArgDescL) = ade_duplicate_desc(Arg, ArgDescL),
        ArgsArg),

    compile_one_argmap(
        Unsorted,
        func(ArgDesc) = ArgDesc ^ argdesc_long,
        func(Long, ArgDescL) = ade_multiple_long(Long, ArgDescL),
        ArgsLong0),

    add_abbreviations(ArgsLong0, ArgsLong),

    compile_one_argmap(
        Unsorted,
        func(ArgDesc) = ArgDesc ^ argdesc_short,
        func(Short, ArgDescL) = ade_multiple_short(Short, ArgDescL),
        ArgsShort),

    Argmaps = argmaps(ArgsArg, ArgsLong, ArgsShort).



% This takes the argument descriptions (as compiled by the condense_properties perdicate for each property list).
% Each one hosts some keys. The keys, that exist in the argument description, are extracted by a key extractor
% function. compile_one_argmap makes a map of each key to the originating argument description. This means that for
% each argdesc, there can be zero to several keys that map to it.
%
% It's possible that there are several copies of the same key in an argdesc. It's also possible that the same key
% occurs in several argdescs. The second case (but not the first one) is an error and causes an exception of type
% arg_desc_error to be thrown. This exception is built by the exception generator function.

:- pred compile_one_argmap(
    list(argdesc)::in,                                  % The argument descriptions (created out of the property
                                                        % lists)
    (func(argdesc) = list(Key))::in,                    % Key extractor function
    (func(Key, list(argdesc)) = arg_desc_error)::in,    % Exception generator function
    map(Key, argdesc)::out                              % The compiled map from Key to the argdesc
) is det.

compile_one_argmap(ArgDescL, KeyExtr, ExcGen, KeyArgDescMap) :-

    % KeyLL: For each argdesc the list of keys that occur in it.
    KeyLL = list.map(KeyExtr, ArgDescL),

    % OccuringKeys: The list of all the keys that occur in any of the argmaps.
    OccuringKeys = list.condense(list.sort_and_remove_dups(KeyLL)),

    % For a given key, get all the argdescs which have that key.
    ArgDescsWithKey =
        (pred(Key::in, ArgDescLOcc::out) is det :-
            list.filter(
                (pred(ArgDesc::in) is semidet :-
                    ( if   list.member(Key, KeyExtr(ArgDesc))
                      then true
                      else fail
                    )),
                ArgDescL,
                ArgDescLOcc)),

    % Get the (unique) key to argdesc mapping, for all keys. For each key that occurs in any Argdesc
    % (OccuringKeys), get the argdescs which have that key. When this are several, then the key isn't unique.
    % That's an error. Throw the corresponding arg_desc_error exception. When there's one, all is well. Add the
    % mapping key->argdesc to the map. When there's none, then the key doesn't map to that argdesc. Don't add it to
    % the map.
    foldl(
        (pred(Key::in, MapIn::in, MapOut::out) is det :-
            ArgDescsWithKey(Key, WithKey),
            (
                WithKey = [],
                MapOut = MapIn
            ;
                WithKey = [OneArgDesc],
                MapOut = map.det_insert(MapIn, Key, OneArgDesc)
            ;
                WithKey = [_,_|_],
                throw(ExcGen(Key, WithKey))
            )),
        OccuringKeys,
        map.init,
        KeyArgDescMap).


:- pred add_abbreviations(map(string, argdesc)::in, map(string, argdesc)::out) is det.

add_abbreviations(ArgMapIn, ArgMapOut) :-
    % All long argument names
    Longs = map.keys(ArgMapIn),

    % For all
    map.foldl(
        (pred(Long::in, ArgDesc::in, !.ArgMap::in, !:ArgMap::out) is det :-
            % Get the list of all prefixes of the long argument.
            solutions(
                (pred(Shorter::out) is nondet :-
                    stringsupp.prefix(Shorter, Long)),
                ShorterL),

            % Filter out all non-unique prefixes.
            list.filter(
                (pred(Shorter::in) is semidet :-
                    prefixof(Shorter, Longs, AbbreviatedL),
                    ( AbbreviatedL = [_],      true
                    ; AbbreviatedL = [],       fail
                    ; AbbreviatedL = [_, _|_], fail
                    )),
                ShorterL,
                ShorterL1),

            % Add the unique prefixes to the long arguments map.
            foldl(
                (pred(Short::in, !.ArgMap1::in, !:ArgMap1::out) is det :-
                    ( if   map.insert(Short, ArgDesc, !.ArgMap1, ArgMapNew)
                      then !:ArgMap1 = ArgMapNew
                      else true
                    )
                ),
                ShorterL1,
                !ArgMap)
        ),
        ArgMapIn,
        ArgMapIn,
        ArgMapOut).



% Determine which subcommand has been chosen by the user. The user can specify a full subcommand or a
% unique prefix thereof. Throws command line errors if it is unknown or ambigious.

:- pred chosen_subcommand(
    string::in,         % The subcommand that the user used on the command line. This can be a full subcommand or
                        % an abbreviation (a prefix) thereof.
    list(string)::in,   % List of all subcommands (without description)
    string::out         % The full subcommand that has been chosen.
) is det.

chosen_subcommand(Prefix, Subcmds, Chosen) :-
    solutions(
        (pred(Subcmd::out) is nondet :-
            list.member(Subcmd, Subcmds),
            stringsupp.prefix(Prefix, Subcmd)),
        Matching
        ),
    (
        Matching = [ Chosen ]
    ;
        Matching = [ ],
        throw(cle_unknown_subcommand(Prefix, Subcmds))
    ;
        Matching = [_, _| _],
        throw(cle_ambigious_subcommand(Prefix, Matching, Subcmds))
    ).




%--------------------------------------------------------------------------------------------------------------
% Parsing the command line

% The list of command line arguments, which the DCG rules process.

:- type clpstate
    ---> clpstate(
        % Number of the first parameter in clpstate_pars
        clpstate_num :: int,
        % The rest of the command line parameters
        clpstate_pars :: list(string),
        % The parsed arguments so far
        clpstate_arguments :: arguments,
        % The parsed non-arguments so far
        clpstate_nonarguments :: list(string),
        % The compiled argument descriptions
        clpstate_argmaps :: argmaps
    ).



:- type checker
    ---> some [T] checker( checker_pred(T) ) => argument(T).


:- type checker_pred(T)
    ---> checker_pred(

            % The third argument (T::out) must be a copy of the second argument (T::in). The caller will then
            % continue with the checker's output argument and won't use in input argument any longer. Thereby it is
            % ensured that the call to the checker won't get optimized away (in any semantics). And the order of
            % execution (first call to checker and then other subsequent calls) is also ensured, so the compiler
            % won't reorder the calls.
            pred(argdesc::in, origin::in, T::in, T::out) is det
         ).



% Get the next parameter from the command line. Returns the parameter and the parameter number.

:- pred el(string::out, int::out, clpstate::in, clpstate::out) is semidet.

el(Par, Num, StateIn, StateOut) :-
    Par = head(clpstate_pars(StateIn)),
    Num = clpstate_num(StateIn),
    StateOut =
        clpstate(
            Num + 1,
            tail(clpstate_pars(StateIn)),
            clpstate_arguments(StateIn),
            clpstate_nonarguments(StateIn),
            clpstate_argmaps(StateIn)
        ).



% Result of the parsearg DCG rule.

:- type parsearg_res
    ---> pa_long(cla, maybe(string))   % found long argument (maybe with argument value)
    ;    pa_short(cla, maybe(string))  % found short argument (maybe with argument value)
    ;    pa_nonarg(string).            % found non-argument parameter



% Get one command line argument. This encompasses one or two arguments, depending on whether the argument takes a
% value.

:- pred parsearg(
    parsearg_res::out,     % the argument, taken from the command line
    clpstate::in,          % command line in
    clpstate::out)         % command line out, with (1 or 2) less arguments
is semidet.

parsearg(Res, !State) :-
    Arguments = clpstate_arguments(!.State),
    el(Par, Num, !State),
    parsearg_1(Res0, string.to_char_list(Par), _),
    (
        % Long argument with value in the same parameter
        Res0 = pa_long_1(Name, yes(Val)),
        lookup_argdesc_long(Arguments, Name, Num, Par, Argdesc),
        Arg = argdesc_cla(Argdesc),
        ( if   argdesc_valname(Argdesc) = yes(_)
          then Res = pa_long(Arg, yes(Val))
          else throw(cle_unexpected_value(Val, Num, Par) : command_line_error)
        )
    ;
        % Long argument without value in the same parameter
        Res0 = pa_long_1(Name, no),
        lookup_argdesc_long(Arguments, Name, Num, Par, Argdesc),
        Arg = argdesc_cla(Argdesc),
        ( if   argdesc_valname(Argdesc) = yes(_)
          then
              ( if   el(Par2, _, !State)
                then Res = pa_long(Arg, yes(Par2))
                else throw(cle_missing_value(Argdesc, Num, Par))
              )
          else Res = pa_long(Arg, no)
        )
    ;
        % Short argument with value in the same parameter
        Res0 = pa_short_1(Name, yes(Val)),
        lookup_argdesc_short(Arguments, Name, Num, Par, Argdesc),
        Arg = argdesc_cla(Argdesc),
        ( if   argdesc_valname(Argdesc) = yes(_)
          then Res = pa_short(Arg, yes(Val))
          else throw(cle_unexpected_value(Val, Num, Par) : command_line_error)
        )
    ;
        % Short argument without value in the same parameter
        Res0 = pa_short_1(Name, no),
        lookup_argdesc_short(Arguments, Name, Num, Par, Argdesc),
        Arg = argdesc_cla(Argdesc),
        ( if   argdesc_valname(Argdesc) = yes(_)
          then
              ( if   el(Par2, _, !State)
                then Res = pa_short(Arg, yes(Par2))
                else throw(cle_missing_value(Argdesc, Num, Par))
              )
          else Res = pa_short(Arg, no)
        )
    ;
        Res0 = pa_nonarg_1(Str),
        Res  = pa_nonarg(Str)
    ).



% Parse the contents of one command line parameter. This includes a possible argument value, which is part of the
% parameter, but doesn't inlcude a possible value in the next parameter.
%
% The following are the possible results:
%
% pa_long(string, maybe(string))
%     A long command line argument ("--...") has been detected. The arguments to pa_long are the command line
%     argument's name and a possible value.
%
% pa_short(char, maybe(string))
%     A short command line argument ("-...") has been detected. The arguments to pa_short are the command line
%     argument's name and a possible value.
%
% pa_nonarg(string)
%     The parameter (not beginning with "-") doesn't contain (the beginning of) a command line argument. Returns
%     the parameter.

% Result of the parsearg_1 DCG rule.
:- type parsearg_res_1
    ---> pa_long_1(string, maybe(string)) % found long argument (maybe with argument value)
    ;    pa_short_1(char, maybe(string))  % found short argument (maybe with argument value)
    ;    pa_nonarg_1(string).             % found non-argument parameter

:- pred parsearg_1(parsearg_res_1::out, list(char)::in, list(char)::out) is det.

parsearg_1(PA) -->
    (
        if   [ '-', '-' ], end

        then { PA = pa_nonarg_1("--") }

        else if [ '-' ], end

        then { PA = pa_nonarg_1("-") }

        else if [ '-', '-', AN0 ]

        then paargname(AN1),
             { AN = [AN0|AN1] },
             (
                 if   [ '=' ]
                 then parest(Rest),
                      { PA = pa_long_1(string.from_char_list(AN), yes(string.from_char_list(Rest))) }
                 else { PA = pa_long_1(string.from_char_list(AN), no) }
             )

        else if [ '-', AN ]

        then parest(Rest),
             (   { Rest = [],
                   PA = pa_short_1(AN, no) }
             ;   { Rest = [_|_],
                   PA = pa_short_1(AN, yes(string.from_char_list(Rest))) }
             )

        else parest(Rest),
             { PA = pa_nonarg_1(string.from_char_list(Rest)) }

     ).



% DCG rule for argument name, inside the parameter.

:- pred paargname(list(char)::out, list(char)::in, list(char)::out) is det.

paargname(AN) -->
    (
        if   [Ch], { Ch \= ('=') }
        then { AN = [Ch|Chars] },
             paargname(Chars)
        else { AN = [] }
    ).


% DCG rule for the rest of a parameter. Meaning, for the argument value.

:- pred parest(list(char)::out, list(char)::in, list(char)::out) is det.

parest(Rest, Rest, []).


% DCG rule for the end of the input.

:- pred end(list(T)::in, list(T)::out) is semidet.

end([], []).


% Create the command line parser (start) state from the list of parameters.

:- pred paras_to_clpstate(
    list(cla)::in,
    argmaps::in,
    list(string)::in,
    map(string, string)::in,
    clpstate::out)
is det.

paras_to_clpstate(ArgL, ArgMaps, CLPs, EnvMap, State) :-

    State = clpstate(
        0,                                                      % clpstate_num
        CLPs,                                                   % clpstate_pars
        arguments(map.init, map.init, ArgMaps, EnvMap, ArgL),   % clpstate_arguments
        [],                                                     % clpstate_nonarguments
        ArgMaps                                                 % clpstate_argdescs
    ).



%----------------------------------------------------------------------------------------------------
% The collected arguments from the command line. This is the main type the user has to deal with.


% Parsed, finished command line parameters.
:- type arguments
    ---> arguments(

        % For arguments that take a value: the values given on the command line
        arguments_values :: map(cla, list(string)),

        % For arguments that don't take a value: how often the argument occurs on the command line
        arguments_times :: map(cla, int),

        % The compiled argument descriptions
        arguments_argmaps :: argmaps,

        % The environment variables
        arguments_env :: map(string, string),

        % The list of all arguments identities. Used for usage info generation.
        arguments_args :: list(cla)
    ).


% Add a long argument that takes a value to the collected arguments
:- pred args_add_long(
    cla::in,                % The command line argument, to which the value is to be added.
    maybe(string)::in,      % Value to add to the argument or parameter to add
    arguments::in,          % Compiled command line arguments in
    arguments::out          % Compiled command line arguments out
) is det.

args_add_long(
    Arg,
    MaybeValue,
    arguments(ValMapIn,  TimesMapIn,  ArgDescs, EnvMap, ArgL),
    arguments(ValMapOut, TimesMapOut, ArgDescs, EnvMap, ArgL)
) :-
    (
        MaybeValue = yes(Value),
        list_add(Arg, Value, ValMapIn, ValMapOut),
        TimesMapOut = TimesMapIn
    ;
        MaybeValue = no,
        int_add(Arg, TimesMapIn, TimesMapOut),
        ValMapOut = ValMapIn
    ).


% Add a short argument that takes a value to the collected arguments
:- pred args_add_short(
    cla::in,                % The command line argument, to which the value is to be added.
    maybe(string)::in,      % Value to add to the argument or parameter to add
    arguments::in,          % Compiled command line arguments in
    arguments::out          % Compiled command line arguments out
) is det.

args_add_short(
    Arg,
    MaybeValue,
    arguments(ValMapIn,  TimesMapIn,  ArgDescs, EnvMap, ArgL),
    arguments(ValMapOut, TimesMapOut, ArgDescs, EnvMap, ArgL)
) :-
    (
        MaybeValue = yes(Value),
        list_add(Arg, Value, ValMapIn, ValMapOut),
        TimesMapOut = TimesMapIn
    ;
        MaybeValue = no,
        int_add(Arg, TimesMapIn, TimesMapOut),
        ValMapOut = ValMapIn
    ).


% Add a long argument that takes a value to the collected arguments
:- pred add_long(
    cla::in,                % The command line argument, to which the value is to be added.
    maybe(string)::in,      % Value to add to the argument or parameter to add
    clpstate::in,           % Compiled command line arguments in
    clpstate::out           % Compiled command line arguments out
) is det.


add_long(Arg, MaybeVal, !State) :-
    ArgumentsIn = clpstate_arguments(!.State),
    args_add_long(Arg, MaybeVal, ArgumentsIn, ArgumentsOut),
    !:State = 'clpstate_arguments :='(!.State, ArgumentsOut).



% Add a short argument that takes a value to the collected arguments
:- pred add_short(
    cla::in,                % The command line argument, to which the value is to be added.
    maybe(string)::in,      % Value to add to the argument or parameter to add
    clpstate::in,           % Compiled command line arguments in
    clpstate::out           % Compiled command line arguments out
) is det.

add_short(Arg, MaybeVal, !State) :-
    ArgumentsIn = clpstate_arguments(!.State),
    args_add_short(Arg, MaybeVal, ArgumentsIn, ArgumentsOut),
    !:State = 'clpstate_arguments :='(!.State, ArgumentsOut).



% Add a non-argument parameter to the result list
:- pred add_nonarg(
    string::in,        % Value to add to the argument or parameter to add
    clpstate::in,      % Compiled command line arguments in
    clpstate::out      % Compiled command line arguments out
) is det.

add_nonarg(Val, !State) :-
    !:State = 'clpstate_nonarguments :='(!.State,
        clpstate_nonarguments(!.State) ++ [Val]).


% Add a value to a map which maps to lists.
:- pred list_add(K::in, V::in, map(K, list(V))::in, map(K, list(V))::out) is det.

list_add(K, V, MapIn, MapOut) :-
    (
        if   map.search(MapIn, K, V0)
        then map.det_update(K, V0 ++ [V], MapIn, MapOut)
        else map.det_insert(K, [V], MapIn, MapOut)
    ).


% Add one to an entry in a map which maps to ints.

:- pred int_add(K::in, map(K, int)::in, map(K, int)::out) is det.

int_add(K, MapIn, MapOut) :-
    (
        if   map.search(MapIn, K, V0)
        then map.det_update(K, V0 + 1, MapIn, MapOut)
        else map.det_insert(K, 1, MapIn, MapOut)
    ).



%--------------------------------------------------------------------------------------------------------------
% 4. Argument query functions


% This is for the case when a switch, or inverse switch, has an environment variable. This is allowed. The value
% is parsed, with the following type as the result type. This is used internally only. It isn't exported to the
% user. See the instance argument(env_switch), below.
:- type env_switch
    ---> ev_yes
    ;    ev_no
    ;    ev_select.


:- instance argument(string) where [
        from_str(Str) = fs_ok(Str)
    ].

:- instance argument(int) where [
        from_str(Str) = Res :-
            ( if   string.to_int(Str, Int)
              then Res = fs_ok(Int)
              else Res = fs_error("Integer expected.")
            )
    ].

:- instance argument(float) where [
        from_str(Str) = Res :-
            ( if   string.to_float(Str, Float)
              then Res = fs_ok(Float)
              else Res = fs_error("Floating point number expected.")
            )
    ].

% This is the parser for values of an environment variable, which has been specified for a switch or a inverse
% switch. For an environment variable called "VAR", the values "yes" and "1" specify that the value of the
% (inverse) switch is "yes". The values "no" and "0" specify that the value of the switch is "no". It doesn't
% matter if the switch is inverse. A present but empty value has the same effect as adding the switch to the
% command line. In other words, it makes the argument value "yes" for an ordinary switch and "no" for an inverse
% switch.
:- instance argument(env_switch) where [
        from_str(Str) = Res :-
            (
                Upper = string.to_upper(Str),
                (
                    if
                        ( Str = "1" ; Upper = "YES" )
                    then
                        Res = fs_ok(ev_yes)
                    else if
                        ( Str = "0" ; Upper = "NO" )
                    then
                        Res = fs_ok(ev_no)
                    else if
                        Str = ""
                    then
                        Res = fs_ok(ev_select)
                    else
                        Res = fs_error("Invalid value " ++ quote_qm(Str) ++ " for environment variable for switch")
                )
            )
    ].


:- pred from_str(
    argdesc::in,        % ArgDesc
    origin::in,         % Where the string form of the argument came from
    string::in,         % String form of the argument
    T::out              % Argument that has been encoded as a string
) is det <= argument(T).

from_str(ArgDesc, Origin, Str, T) :-
    Res = from_str(Str),
    (
        Res = fs_ok(T)
    ;
        Res = fs_error(Msg),
        throw(cle_malformed_value(ArgDesc, Origin, Str, Msg))
    ).


% Get the number of times, which an argument occurs *on the command line*. This works for value-taking arguments
% and for arguments that don't take a value. This doesn't count default arguments and arguments taken from an
% environment variable.

:- func get_times(arguments, cla) = int.

get_times(Arguments, Arg) = Times :-
    (
        if       map.search(Arguments ^ arguments_times, Arg, Times0)
        then     Times = Times0
        else if  map.search(Arguments ^ arguments_values, Arg, Values)
        then     Times = length(Values)
        else     Times = 0
    ).


% Get the minimum allowed number of times, which an argument can occur, by arguments and cla.

:- func get_min_times(arguments, cla) = int.

get_min_times(Arguments, Arg) = Min :-
    (
        if
            map.search(Arguments ^ arguments_argmaps ^ argmaps_cla, Arg, ArgDesc)
        then
            Min = get_min_times(ArgDesc)
        else
            throw(ade_unknown_arg(Arg))
    ).


% Get the minimum allowed number of times, which an argument can occur, by argdesc.

:- func get_min_times(argdesc) = int.

get_min_times(ArgDesc) = Min :-
    MMin = ArgDesc ^ argdesc_min,
    (   MMin = yes(Min - _)
    ;   MMin = no,
        Min = 0
    ).



% Get the maximum allowed number of times, which an argument can occur, if any. This depends on the argument
% description.

:- func get_max_times(arguments, cla) = max_spec.

get_max_times(Arguments, Arg) = Max :-
    Max = get_max_times(get_argdesc(Arguments, Arg)).


:- func get_max_times(argdesc) = max_spec.

get_max_times(ArgDesc) = Max :-
    MMax = ArgDesc ^ argdesc_max,
    (   MMax = yes(Max0 - _),
        Max  = number(Max0)
    ;   MMax = no,
        Max = unlimited
    ).



% Get the argument description of an argument, by arguments(Arg) an Arg.

:- func get_argdesc(arguments, cla) = argdesc.

get_argdesc(Arguments, Arg) = ArgDesc :-
    (
        if map.search(Arguments ^ arguments_argmaps ^ argmaps_cla, Arg, ArgDesc0)
        then
            ArgDesc = ArgDesc0
        else
            throw(ade_unknown_arg(Arg))
    ).


% Get the unparsed value of the environment variable specified with the prop_env property, if any.

:- func get_env(arguments, cla) = maybe(string).

get_env(Arguments, Arg) = MEnv :-
    (
        if   get_env(Arguments, Arg, Env)
        then MEnv = yes(Env)
        else MEnv = no
    ).

:- pred get_env(arguments::in, cla::in, string::out) is semidet.

get_env(Arguments, Arg, Env) :-
    ArgDesc = get_argdesc(Arguments, Arg),
    yes(Envname) = ArgDesc ^ argdesc_env,
    map.search(Arguments ^ arguments_env, Envname, Env).


% Verify the arguments given on the command line for consistence with the argument descriptions. We return a copy
% of Arguments so verify_args or verify_arg won't be optimized away (even in non-default semantics).

:- pred verify_args(
    list(argdesc)::in,
    arguments::in,
    arguments::out)
is det.

verify_args(ArgDescs, Arguments, Arguments1) :-
    map(verify_arg(Arguments), ArgDescs, ArgDescs1),
    Arguments1 = 'arguments_args :='(
        Arguments,
        map(func(AD) = argdesc_cla(AD), ArgDescs1)).



% Calculate the number of times that an argument occurs. When it's specfied in the command line, then this
% determines the number of occurences. The environment variable (if any) and the default value (if any) don't
% count, then. When there are no occurences on the command line, then there can still be one occurence in an
% environment variable (if any) and, if there isn't, there can still be one occurence from a default value.

:- pred get_times_occuring(
    arguments::in,
    argdesc::in,
    int::out
) is det.

get_times_occuring(Arguments, ArgDesc, Times) :-
    Arg = ArgDesc ^ argdesc_cla,

    % Count the arguments which are given on the command line.
    TimesCL = get_times(Arguments, Arg),

    % Count the occurance of an environment variable.
    TimesEnv = (
        if  TimesCL = 0,
            ArgDesc ^ argdesc_env = yes(_),
            get_env(Arguments, Arg) = yes(_)
        then 1
        else 0),

    % Count the occurance of an default value.
    TimesDefault = (
        if  TimesCL = 0,
            TimesEnv = 0,
            ArgDesc ^ argdesc_default = yes(_)
        then 1
        else 0),

    Times = TimesCL + TimesEnv + TimesDefault.



% Verify the consistence of the description of one argument with the occurences of that argument on the command
% line, in an environmelt variable (if any) and in a default value (if any). We return a copy of the ArgDesc input
% argument in order to keep the compiler from optimizing verify_arg away (in non-default semantics).

:- pred verify_arg(
    arguments::in,
    argdesc::in,
    argdesc::out)
is det.

verify_arg(Arguments, ArgDesc, ArgDesc1) :-
    get_times_occuring(Arguments, ArgDesc, Times),

    Min = min_possible(ArgDesc),
    Max = max_possible(ArgDesc),

    ( if   Times < Min
      then throw(cle_too_few_occurrences(ArgDesc, Min, Times))
      else true
    ),

    (
        Max = number(Max1),
        ( if   Times > Max1
          then throw(cle_too_many_occurrences(ArgDesc, Max1, Times))
          else true
        )
    ;
        Max = unlimited
    ),

    % If we have both, a default and a checker, then check the default.
    (
        if  ArgDesc ^ argdesc_default = yes(default_wrapper(Default)),
            ArgDesc ^ argdesc_checker = yes(_Checker)
        then
            check_value_with_checker("", ArgDesc, origin_default, Default, _)
        else true
    ),

    ArgDesc1 = ArgDesc.



% Check if the minimum number of occurences of an argument, as determined by the argument description, matches
% what is expected by an argument query function. Otherwise throw an exception.

:- pred check_min_possible(string::in, argdesc::in, int::in) is det.

check_min_possible(QueryFunction, ArgDesc, MinMustBe) :-
    (
        if   min_possible(ArgDesc) = MinMustBe
        then true
        else throw(ade_min_number_of_occurrences_mismatch(ArgDesc, MinMustBe, QueryFunction))
    ).


% The maximum number of occurences of an argument can be a certain number, or unlimieted.

:- type max_spec
    ---> number(int)
    ;    unlimited.


% Check if the maximum number of occurences of an argument, as determined by the argument description, matches
% what is expected by an argument query function. Otherwise throw an exception.

:- pred check_max_possible(string::in, argdesc::in, int::in) is det.

check_max_possible(QueryFunction, ArgDesc, MaxMustBe) :-
    (
        if   max_possible(ArgDesc) = number(MaxMustBe)
        then true
        else throw(ade_max_number_of_occurrences_mismatch(ArgDesc, MaxMustBe, QueryFunction))
    ).


% Check if the specified argument takes or doesn't take a value. Throws an exception when the argument doesn't
% fulfill the expectation about whether an argument is taken.

:- pred check_takes_value(
    string::in,         % Name of the calling argument query function
    argdesc::in,        % Argument description of the argument
    bool::in)           % If it is expected that the argument takes a value
is det.

check_takes_value(QueryFunction, ArgDesc, ExpectingValue) :-
    (   if ArgDesc ^ argdesc_valname = yes(_)
        then TakesValue = yes
        else TakesValue = no
    ),
    (
        if   TakesValue \= ExpectingValue
        then throw(ade_takes_value_mismatch(ArgDesc, TakesValue, ExpectingValue, QueryFunction))
        else true
    ).


% Identify the type of the values which the checker is for. We deconstruct the type description of the
% checker_pred's type and take the type of the second argument. That's the type of the value, which the checker
% checks.

:- pred get_checker_pred_arg_type(checker_pred(T)::in, type_desc.type_desc::out) is det.

get_checker_pred_arg_type(CheckerPred, Type) :-
    type_desc.type_ctor_and_args(
        type_desc.type_of(CheckerPred),
        _TypeCtorDesc,
        ArgTypeL),
    (
        if   ArgTypeL = [ Type0 ]
        then Type = Type0
        else unexpected($pred, "Type description doesn't match type. ArgTypeL=" ++
                        string.string(ArgTypeL))
    ).


% Get the list of (type-converted) values of an argument.
:- pred get_argdesc_values(
    arguments::in,         % The compiled arguments
    cla::in,               % The argument in question
    argdesc::out,          % The argument's description
    list(string)::out)     % The list of values, specified on the command line
is det.

get_argdesc_values(Arguments, Arg, ArgDesc, Values) :-
    Valmap  = Arguments ^ arguments_values,
    ArgMaps = Arguments ^ arguments_argmaps,
    Argmap  = ArgMaps ^ argmaps_cla,
    (
        if   map.search(Argmap, Arg, ArgDesc0)
        then ArgDesc = ArgDesc0
        else throw(ade_unknown_arg(Arg))
    ),
    (
        if   map.search(Valmap, Arg, Vals)
        then Values = Vals
        else Values = []
    ).



% Convert some values from the string form (as given on the command line or environment variable) to the argument
% form. This is just a map of from_str over the strings.

:- pred convert_values(argdesc::in, origin::in, list(string)::in, list(T)::out) is det <= argument(T).

convert_values(ArgDesc, Origin, Values1, Values2) :-
    map(from_str(ArgDesc, Origin), Values1, Values2).



% For an argument that has a checker, check the value with it. Wie return a copy of the given value, so the
% compiler won't optimize away the check, and won't reorder clauses.

:- pred check_value_with_checker(
    string::in,         % Name of the querying function
    argdesc::in,        % Argument description. Here's the checker, if any.
    origin::in,         % Origin of the value that's to be checked
    T::in,              % Value to be checked
    T::out)             % Same but scrutinized value
is det.

check_value_with_checker(QueryFunction, ArgDesc, Origin, Val0, Val) :-
    % When the argument has a checker, then apply it.
    MChecker = ArgDesc ^ argdesc_checker,
    (
        MChecker = yes(checker(CheckerPred0)),
        (
          % When the checker's type matches the type of the value we are querying, then all is right. We apply
          % the checker. When not, we have the situation that the type of the checker doesn't match the type of
          % the checked values. We throw an argument description error.
          if   dynamic_cast(CheckerPred0, CheckerPred)
          then CheckerPred = checker_pred(Checker),
               Checker(ArgDesc, Origin, Val0, Val)
          else get_checker_pred_arg_type(CheckerPred0, TypeDesc),
               throw(ade_type_mismatch_checker(ArgDesc, QueryFunction, Origin, TypeDesc, type_desc.type_of(Val)))
        )
    ;
        MChecker = no,
        Val = Val0
    ).



% For an argument that has a checker, check all the values in the list. Wie return a copy of the given value, so
% the compiler won't optimize away the check, and won't reorder clauses.

:- pred check_values_with_checker(
    string::in,         % Name of the querying function
    argdesc::in,        % Argument description. Here's the checker, if any.
    origin::in,         % Origin of the value that's to be checked
    list(T)::in,        % Value in
    list(T)::out)       % Same but scrutinized value out.
is det.

check_values_with_checker(QueryFunction, ArgDesc, Origin, Vals0, Vals) :-
    map(check_value_with_checker(QueryFunction, ArgDesc, Origin), Vals0, Vals).



%----------------------------------------------------------------------------------------------------

% This is for select/4. See there.

:- pred chain(
    pred(origin, list(T))::pred(out, out) is det,
    origin::in, origin::out,
    list(T)::in, list(T)::out
) is det <= argument(T).

chain(Pred, !Origin, !Values) :-
    (
        !.Values = [],
        Pred(Origin1, Values1),
        (
            Values1 = []
        ;
            Values1 = [_|_],
            !:Origin = Origin1,
            !:Values = Values1
        )
    ;
        !.Values = [_|_]
    ).



% Try to get argument values from three sources:
%
% * the command line
% * an environment variable (if specified by prop_env)
% * a default values (if specified by prop_default)
%
% The first source, which yields some arguments (at least one), wins and determines the values of the argument. The
% rest of the sources is ignored. The second and third source yield a one-element list. The first one can yield any
% number of values.

:- pred select(
    string::in,                 % Name of the function which is being used to query argument values
    arguments::in,              % Compiled command line arguments
    cla::in,                    % Which argument to query
    list(T)::out                % Values of the argument
) is det <= argument(T).

select(QueryFunction, Arguments, Arg, Values) :-
    ArgDesc = get_argdesc(Arguments, Arg),
    check_takes_value($pred, ArgDesc, yes),

    some [!Vals, !Origin] (
        !:Vals = [],
        !:Origin = origin_none,
        chain(get_from_command_line(QueryFunction, Arguments, Arg), !Origin, !Vals),
        chain(get_from_environment(QueryFunction, Arguments, Arg), !Origin, !Vals),
        chain(get_from_default(QueryFunction, Arguments, Arg), !.Origin, _, !Vals),
        Values = !.Vals
    ).



% Get the argument values from the command line.

:- pred get_from_command_line(
    string::in,                 % Name of the function which is being used to query argument values
    arguments::in,              % The compiled arguments
    cla::in,                    % The argument in question
    origin::out,                % This is origin_command_line and specifies that the values come from there.
    list(T)::out                % List of values from the command line.
) is det <= argument(T).

get_from_command_line(QueryFunction, Arguments, Arg, origin_command_line, Values) :-
    get_argdesc_values(Arguments, Arg, ArgDesc, Values0),
    convert_values(ArgDesc, origin_command_line, Values0, Values1),
    check_values_with_checker(QueryFunction, ArgDesc, origin_command_line, Values1, Values).


% Get (a list of) one argument value from an environment variable, if one has been defined with prop_env. Otherwise
% returns the empty list.

:- inst one_only_list == bound([ground] ; []).

:- pred get_from_environment(
    string::in,                 % Name of the function which is being used to query argument values
    arguments::in,              % The compiled arguments
    cla::in,                    % The argument in question
    origin::out,
    list(T)::out(one_only_list)
) is det <= argument(T).

get_from_environment(QueryFunction, Arguments, Arg, origin_env, Values) :-
    ArgDesc = get_argdesc(Arguments, Arg),
    (
        if
            get_env(Arguments, Arg, Env)
        then
            from_str(ArgDesc, origin_env, Env, Val0),
            check_value_with_checker(QueryFunction, ArgDesc, origin_env, Val0, Val),
            Values = [Val]
        else
            Values = []
    ).



% Get (a list of) one argument value from a default, if one has been defined with prop_default. Otherwise returns
% the empty list.
%
% This does NOT check the default value with the checker (if one had been established). So the default isn't
% restricted by it.

:- pred get_from_default(
    string::in,                 % Name of the function which is being used to query argument values
    arguments::in,              % The compiled arguments
    cla::in,                    % The argument in question
    origin::out,
    list(T)::out
) is det <= argument(T).

get_from_default(QueryFunction, Arguments, Arg, origin_default, Values) :-
    ArgDesc = get_argdesc(Arguments, Arg),
    MDef = ArgDesc ^ argdesc_default,
    (
        % The argument has a default value.
        MDef = yes(default_wrapper(Default)),
        (   if   private_builtin.typed_unify(Default, Val0)
            then Values = [Val0]
            else
                type_desc.type_ctor_and_args(type_desc.type_of(Values), _, TArgs),
                ( TArgs = [ Type ],
                  throw(ade_type_mismatch_default(ArgDesc, QueryFunction, type_desc.type_of(Default), Type))
                ; TArgs = [],
                  unexpected($pred, "No type arguments. Should be one.")
                ; TArgs = [_,_|_],
                  unexpected($pred, "Too many type arguments. Should be one.")
                )
        )
    ;
        % The argument has no default value.
        MDef = no,
        Values = []
    ).



%----------------------------------------------------------------------------------------------------

% The acutal occurences of an argument is determined by the occurences on the command line, a possible default
% value and a possible default value.
%
% The environment variable (if prop_env has been used), gets used only, when there are no such arguments on the
% command line. The default value (if prop_default has been used), gets used only, when there are none on the
% command line and there's no valid environment variable, either.


% Calculates the minimum number of occurences of an argument, which is possible, from the specified argument
% description.

:- func min_possible(argdesc) = int.

min_possible(ArgDesc) = Min :-
    MDef = ArgDesc ^ argdesc_default,
    ArgDescMin = get_min_times(ArgDesc),
    (
        if
            (
                % There must be at least one *on the command line*. So we always get one, at least.
                ArgDescMin = 1,
                Min0 = 1
            ;
                % There doesn't need to by any on the command line, but there is a default value, which will come
                % into play. So at least one value is possible (command line or default).
                ArgDescMin = 0,
                MDef = yes(_),
                Min0 = 1
            ;
                % There doesn't need to by any on the command line and there's no default either. So we can have
                % zero occurences.
                ArgDescMin = 0,
                MDef = no,
                Min0 = 0
            )
        then
            Min = Min0

        else if
            ArgDescMin > 1,
            (
                % More than one needed. That's okay as long as there is no default. The default can supply only one
                % argument.
                MDef = no,
                Min0 = ArgDescMin
            ;
                % More than one needed and there's a default. The default can supply only one argument, so this is
                % faulty. The case should have been noticed and caused an error when the argument description was
                % condensed from the argument's property list.
                MDef = yes(_),
                unexpected($pred, "Argument description with min > 1 and default value found.")
            )
        then
            Min = Min0

        else
            % If we reach here, the minumum is negative. That's a bug.
            unexpected($pred, "Argument description with min = " ++ string.string(ArgDescMin) ++ " found.")
    ).



% Calculates the maximum number of occurences of an argument, which is possible, from the specified argument
% description. The maximum comes from the properties list, and can be zero. In this case, a possible environment
% variable and a default value, override this maximum of zero. It becomes one, namely the one, namely the in the
% environment or the one in the default value.

:- func max_possible(argdesc) = max_spec.

max_possible(ArgDesc) = Max :-
    MEnv = ArgDesc ^ argdesc_env,
    MDef = ArgDesc ^ argdesc_default,
    ArgDescMax = get_max_times(ArgDesc),
    (
        if
            (
                ArgDescMax = number(0),
                MDef = yes(_),
                MEnv = yes(_),
                Max0 = number(1)
            ;
                ArgDescMax = number(0),
                MDef = yes(_),
                MEnv = no,
                Max0 = number(1)
            ;
                ArgDescMax = number(0),
                MDef = no,
                MEnv = yes(_),
                Max0 = number(1)
            ;
                ArgDescMax = number(0),
                MDef = no,
                MEnv = no,
                Max0 = number(0)
            )
        then
            Max = Max0
        else
            Max = ArgDescMax
    ).



%----------------------------------------------------------------------------------------------------


arg_value(Arguments, Arg) = Val :-
    ArgDesc = get_argdesc(Arguments, Arg),
    check_takes_value($pred, ArgDesc, yes),

    check_min_possible($pred, ArgDesc, 1),
    check_max_possible($pred, ArgDesc, 1),

    select("arg_value/2", Arguments, Arg, Values),

    (
        Values = [],
        throw(cle_too_few_occurrences(ArgDesc, 1, 0))
    ;
        Values = [ Val ]
    ;
        % Several occurrences of the argument on the command line. This is faulty, since we're expecting one
        % occurence when using the arg_value/2 function.
        Values = [_, _| _],
        throw(cle_too_many_occurrences(ArgDesc, 1, length(Values)))
    ).



arg_maybe_value(Arguments, Arg) = MVal :-
    ArgDesc = get_argdesc(Arguments, Arg),
    check_takes_value($pred, ArgDesc, yes),

    check_min_possible($pred, ArgDesc, 0),
    check_max_possible($pred, ArgDesc, 1),

    select("arg_maybe_value/2", Arguments, Arg, Values),

    (
        % One value for the argument is given.
        Values = [ Val0 ],
        MVal = yes(Val0)
    ;
        % No values for the argument are given.
        Values = [],
        MVal = no
    ;
        % Several occurrences of the argument on the command line. This is faulty, since we're expecting at most
        % one occurence when using the arg_maybe_value/2 function.
        Values = [_, _| _],
        throw(cle_too_many_occurrences(ArgDesc, 1, length(Values)))
    ).



arg_values(Arguments, Arg) = Values :-
    ArgDesc = get_argdesc(Arguments, Arg),
    check_takes_value($pred, ArgDesc, yes),

    select("arg_values/2", Arguments, Arg, Values),

    Count = length(Values),
    (
        if      yes(Min - _) = argdesc_min(ArgDesc),
                Count < Min
        then    throw(cle_too_few_occurrences(ArgDesc, Min, Count))

        else if yes(Max - _) = argdesc_max(ArgDesc),
                Count > Max
        then    throw(cle_too_many_occurrences(ArgDesc, Max, Count))

        else    true
    ).



arg_switch(Arguments, Arg) = Val :-
    ArgDesc = get_argdesc(Arguments, Arg),

    check_min_possible($pred, ArgDesc, 0),
    check_max_possible($pred, ArgDesc, 1),

    check_takes_value($pred, ArgDesc, no),

    % How many times the switch has been given on the command line. This must be once, at most. "Specified" is
    % whether the switch is present on the command line.
    Times = get_times(Arguments, Arg),
    (
        if   Times > 1
        then throw(cle_too_many_occurrences(ArgDesc, 1, Times))
        else if Times = 1
        then Specified = yes
        else Specified = no
    ),

    % Calculate the meaning of an occurence of the switch on the command line. For a normal switch, the meaning of
    % an occurence of the switch is yes/selected. For an inverse switch, it is no/not selected.
    Inverse0 = ArgDesc ^ argdesc_inverse,
    (
        Inverse0 = yes(yes),
        Meaning = no
    ;
        Inverse0 = yes(no),
        Meaning = yes
    ;
        Inverse0 = no,
        throw(ade_not_a_switch(Arg, $pred))
    ),

    (
        % Switch specified on the command line.
        Specified = yes,
        Val = Meaning
    ;
        % The switch hasn't been used on the command line. Check if there's an environment variable
        % which determines if the switch is on or off.
        Specified = no,
        get_from_environment($pred, Arguments, Arg, _, ValuesOne : list(env_switch)),
        (
            % Switch turned on by environment variable. (ENVVAR=1 or ENVVAR=yes)
            ValuesOne = [ev_yes],
            Val = yes
        ;
            % Switch turned off by environment variable. (ENVVAR=0 or ENVVAR=no)
            ValuesOne = [ev_no],
            Val = no
        ;
            % Switch is selected by environment variable. (ENVVAR is the empty string)
            ValuesOne = [ev_select],
            Val = Meaning
        ;
            % The environment variable isn't used and the switch isn't specified on the command line.
            ValuesOne = [],
            Val = bool.not(Meaning)
        )
    ).



arg_times(Arguments, Arg) = Times :-
    Times = get_times(Arguments, Arg).



%--------------------------------------------------------------------------------------------------------------
% Parsers and parser frontends

% Generic parser for the command line parameters. The parameter list and the environment variables are specified
% with arguments.
:- pred clparse_full(
    % The command line parser to use
    pred(clpstate, clpstate)::pred(in, out) is det,

    % The argument descriptions
    assoc_list(cla, list(property))::in,

    % The command line parameters
    list(string)::in,

    % The environment variables
    io.environment_var_map::in,

    % The recognized command line arguments
    arguments::out,

    % The non-argument command line parameters
    list(string)::out)
is det.


% Generic parser for the command line parameters. The parameter list and the environment variables are obtained
% with IO actions.
:- pred clparse(
    % The command line parser to use
    pred(clpstate, clpstate)::pred(in, out) is det,

    % The argument descriptions
    assoc_list(cla, list(property))::in,

    % The recognized command line arguments
    arguments::out,

    % The non-argument command line parameters
    list(string)::out,

    io::di, io::uo)
is det.


clparse_full(
    Parser,             % in
    ArgsProps,          % in
    CLPs,               % in
    EnvMap,             % in
    Arguments1,         % out
    NonArguments)       % out
:-
    % Do everything except for the verification. This is needed by the "program" predicate.
    clparse_full_1(Parser, ArgsProps, CLPs, EnvMap, Arguments, NonArguments, ArgDescs),

    % Verify the encountered arguments.
    verify_args(ArgDescs, Arguments, Arguments1).



% Parse the command line arguments, but don't verify them yet.

:- pred clparse_full_1(
    % The command line parser to use
    pred(clpstate, clpstate)::pred(in, out) is det,

    % The argument descriptions
    assoc_list(cla, list(property))::in,

    % The command line parameters
    list(string)::in,

    % The environment variables
    io.environment_var_map::in,

    % The recognized command line arguments
    arguments::out,

    % The non-argument command line parameters
    list(string)::out,

    % Compiled arguments
    list(argdesc)::out
) is det.

clparse_full_1(
    Parser,             % in
    ArgsProps,          % in
    CLPs,               % in
    EnvMap,             % in
    Arguments,          % out
    NonArguments,       % out
    ArgDescs)           % out
:-
    % Make the compiled arguments (three maps, which are encapsulated in the argmaps type), from the assoc_list of
    % arguments and their properties.
    map((pred((Arg - PropertyList)::in, ArgDesc::out) is det :-
                condense_properties(Arg, PropertyList, ArgDesc)),
        ArgsProps,
        ArgDescs : list(argdesc)),
    compile_argmaps(ArgDescs, Argmaps),

    % Get the list of all arguments in the order given in the argument descriptions list.
    map(pred((Arg - _)::in, Arg::out) is det :- true, ArgsProps, ArgL),

    % Parse the command line parameters
    paras_to_clpstate(ArgL, Argmaps, CLPs, EnvMap, StateIn),
    Parser(StateIn, StateOut),

    % Exctract the result from the clpstate.
    Arguments = clpstate_arguments(StateOut),
    NonArguments = clpstate_nonarguments(StateOut).




clparse(Parser, ArgDescL, Arguments, NonArguments, !IO) :-
    io.command_line_arguments(CLPs, !IO),
    io.environment.get_environment_var_map(EnvMap, !IO),
    clparse_full(Parser, ArgDescL, CLPs, EnvMap, Arguments, NonArguments).



:- pred parse_gnu(
    clpstate::in,
    clpstate::out)
is det.


colipa(ArgDescL, Arguments, NonArguments, !IO) :-
    clparse(parse_gnu, ArgDescL, Arguments, NonArguments, !IO).


colipa1(ArgDescL, CLP, Env, Arguments, NonArguments) :-
    clparse_full(parse_gnu, ArgDescL, CLP, Env, Arguments, NonArguments).


parse_gnu -->
    (
       if parsearg(Res)
       then
           (
               { Res = pa_long(Arg, MaybeVal) },
               add_long(Arg, MaybeVal)
           ;
               { Res = pa_short(Arg, MaybeVal) },
               add_short(Arg, MaybeVal)
           ;
               { Res = pa_nonarg(NonArg) },
               (
                   if   { NonArg = "--" }
                   then rest_pars
                   else add_nonarg(NonArg)
               )
           ),
           parse_gnu
       else
           { true }
    ).


% A parser that parses all the rest of the command line as non-argument parameters.

:- pred rest_pars(clpstate::in, clpstate::out) is det.

rest_pars -->
    (
        if
            el(Par, _)
        then
            add_nonarg(Par)
        else
            { true }
    ).



%--------------------------------------------------------------------------------------------------------------
% Error message generation

:- func in_para(string) = string.

in_para(Para) =
    (
        if string.sub_string_search(Para, "=", _)
        then "It occurs in parameter " ++ quote_qm(Para)
        else "It occurs "
    ).


:- func in_number(int) = string.

in_number(ParaNo) =
    " in parameter number " ++ string.from_int(ParaNo+1) ++ ". ".


:- func in_para_number(string, int) = string.

in_para_number(Para, ParaNo) =
    ". " ++
    in_para(Para) ++
    in_number(ParaNo).


% An unknown short command line argument.
cle_message(cle_unknown_short_arg(Short, ParaNo, Para)) =
    "Unknown command line argument " ++ quote_qm("-" ++ string.from_char(Short)) ++
    (
        if length(Para) > 2
        then " in " ++ quote_qm(Para)
        else ""
    ) ++
    in_number(ParaNo).

% An unknown long command line argument.
cle_message(cle_unknown_long_arg(Long, ParaNo, Para)) =
    "Unknown command line argument " ++
    quote_qm("--" ++ Long) ++
    in_para_number(Para, ParaNo).

% An abbreviated long command line argument, which is ambigious.
cle_message(cle_ambigious_abbrev(Long, AbbreviatedL, ParaNo, Para)) =
    "Ambigious abbreviation " ++ quote_qm("--" ++ Long) ++
    " of a command line argument. It could mean any of the following: " ++
    human_readable(map((func(Abbr) = "--" ++ Abbr), AbbreviatedL)) ++
    ". " ++
    in_para_number(Para, ParaNo).

% Missing value of an argument that takes a value. This can happen only at the end of the command line.
cle_message(cle_missing_value(ArgDesc, ParaNo, Para)) =
    "Missing argument value for the argument " ++
    quote_qm(argname(ArgDesc)) ++
    " at the end of the command line" ++
    in_para_number(Para, ParaNo).

% There's a value for an argument, which doesn't take an value. This can only happen, when the argument
% consist of only one parameter.
cle_message(cle_unexpected_value(Value, ParaNo, Para)) =
    "Unexpected argument value - there's the value " ++ quote_qm(Value) ++
    " given for a command line argument that doesn't take a value. " ++
    in_para_number(Para, ParaNo).

% Parse error in the value of a command line argument
cle_message(cle_malformed_value(ArgDesc, Origin, MalformedValue, ParseErrorMsg)) = Msg :-
    Msg1 = "Malformed value " ++ quote_qm(MalformedValue) ++ " for the command line argument " ++
           argname(ArgDesc) ++ ": " ++ ParseErrorMsg,
    (
        Origin = origin_command_line,
        Msg2 = ""
    ;
        Origin = origin_env,
        Msg2 = " The value comes from the " ++ get_argdesc_env(ArgDesc) ++ " environment variable."
    ;
        Origin = origin_default,
        unexpected($pred, "Default value type error, which should have been detected before.")
    ;
        Origin = origin_none,
        unexpected($pred, "No origin.")
    ),
    Msg = Msg1 ++ Msg2.


% An argument value could be parsed, but it isn't inside the valid range
cle_message(cle_invalid_value(ArgDesc, InvalidValue, Origin, MParseErrorMsg)) = Msg :-
    Msg1 = "Invalid value " ++ string.string(InvalidValue) ++ " for command line argument " ++ argname(ArgDesc) ++
           ( if MParseErrorMsg = yes(Msg3) then ": " ++ Msg3 else "."),
    ( Origin = origin_command_line,
      Msg2 = ""
    ; Origin = origin_default,
      Msg2 = " The value is the default for the argument."
    ; Origin = origin_none,
      unexpected($pred, "No origin.")
    ; Origin = origin_env,
      MEnv = ArgDesc ^ argdesc_env,
      ( MEnv = yes(Env),
        Msg2 = " The value comes from the " ++ quote_qm(Env) ++ " environment variable."
      ; MEnv = no,
        unexpected($pred, "ArgDesc doesn't match Origin")
      )
    ),
    Msg = Msg1 ++ Msg2.

% The number of occurrences of the argument is too low.
cle_message(cle_too_few_occurrences(ArgDesc, MinAllowed, Found)) =
    (
        if   MinAllowed = 1
        then "The " ++ argname(ArgDesc) ++ " command line argument must be specified."
        else "The number of occurrences of the command line argument " ++ argname(ArgDesc) ++ " is too low. Found "
             ++ int_to_string(Found) ++ " occurences. At least " ++ int_to_string(MinAllowed) ++
             " occurences of the argument must be given."
    ).

% The number of occurrences of the argument is too large.
cle_message(cle_too_many_occurrences(ArgDesc, MaxAllowed, Found)) =
    (
        if   MaxAllowed = 1
        then "The " ++ argname(ArgDesc) ++ " command line argument can be specified only once. " ++
             "Found " ++ int_to_string(Found) ++ " occurences."
        else "The number of occurrences of the command line argument " ++ argname(ArgDesc) ++
             " is too high. Found " ++ int_to_string(Found) ++ " occurences. At most " ++
             int_to_string(MaxAllowed) ++
             " occurences of the argument may be given."
    ).

% There has been no subcommand found, but it is required. That's for programs which make use of the
% "program" predicate.
cle_message(cle_no_subcommand(Msg)) = Msg.

% No or several occurences of arguments in a group, where exactly one was expected.
cle_message(cle_args_one(_, _, Msg)) = Msg.

% No occurence of an argument in a group, where at least one was expected.
cle_message(cle_args_at_least_one(_, _, Msg)) = Msg.

% Several occurence of arguments in a group, where at most one was expected.
cle_message(cle_args_at_most_one(_, _, Msg)) = Msg.

% The user has chosen an unknown subcommand.
cle_message(cle_unknown_subcommand(Chosen, Subcmds)) =
    "Unknown subcommand " ++ quote_qm(Chosen) ++ ". The available subcommands are: "
    ++ human_readable(Subcmds) ++ ".".

% The user has abbreviated the subcommand and this made it ambigious.
cle_message(cle_ambigious_subcommand(Prefix, Possible, _Subcmds)) =
    "Ambigious abbreviation " ++ quote_qm(Prefix) ++ " of the subcommand. Possible completed forms are: "
    ++ human_readable(Possible) ++ ".".



% An argument is defined several times.
ade_message(ade_duplicate_desc(Arg, ArgDescL)) =
    "The " ++ string.string(Arg) ++ " command line argument is defined more than once. " ++
    "The command line arguments in question are: " ++ argsname(ArgDescL).

% The argument doesn't have any (long or short) names.
ade_message(ade_no_name(Arg)) =
    "The " ++ string.string(Arg) ++ " argument doesn't have any (long or short) names.".

% An argument is missing in the arguments descripton assoc_list
ade_message(ade_unknown_arg(Arg)) =
    "Unknown command line argument. The argument " ++ string.string(Arg) ++
    " isn't defined. Add it to the list of the argument descriptions.".

% Several properties in an argument property list conflict with each other.
ade_message(ade_conflict(Arg, PropL)) =
    "Properties of the """ ++ string.string(Arg) ++ """ argument conflict with each other: " ++
    human_readable(PropL).

% The type of the default value of an argument doesn't match the type of the value, which is queried by
% an arg query function. Use type_desc.type_name/1 to get the names of the two types.
ade_message(ade_type_mismatch_default(ArgDesc, QueryFunction, TypeDescDefault, TypeDescQueried)) =
    "The type of the default value of the " ++ string.string(ArgDesc ^ argdesc_cla) ++
    " argument doesn't match the type of the value. The former is defined with the prop_default/1 property. " ++
    "The latter is defined by the query function " ++ QueryFunction ++
    ". The type of the default value is " ++ type_desc.type_name(TypeDescDefault) ++
    ", whereas the queried type is " ++ type_desc.type_name(TypeDescQueried) ++ ".".

% The type of an argument value checker argument doesn't match the type of the value, which is queried by
% a query function. Use type_desc.type_name/1 to get the names of the two types.
ade_message(ade_type_mismatch_checker(ArgDesc, QueryFunction, Origin, TypeDescChecker, TypeDescQueried)) = Msg :-
    Msg1 = "The type of the checker for the argument " ++ string.string(ArgDesc ^ argdesc_cla) ++
           " doesn't match the type of the ",
    (
        Origin = origin_command_line,
        Msg2 = "value, which is queried by the query function " ++ QueryFunction ++ ". "
    ;
        Origin = origin_default,
        Msg2 = "default value. "
    ;
        Origin = origin_none,
        unexpected($pred, "No origin.")
    ;
        Origin = origin_env,
        MEnv = ArgDesc ^ argdesc_env,
        (
            MEnv = yes(Env),
            Msg2 = "value, which is taken from the " ++ quote_qm(Env) ++ " environment variable. "
        ;   MEnv = no,
            unexpected($pred, "Value has origin environment variable, but prop_env isn't set.")
        )
    ),
    Msg3 = "The checker is for values of the type " ++ type_desc.type_name(TypeDescChecker) ++
           ", whereas the type of the value is " ++ type_desc.type_name(TypeDescQueried) ++ ".",
    Msg = Msg1 ++ Msg2 ++ Msg3.


% The minimum allowed number of occurences of the argument, as specified in the argument properties,
% doesn't match the function which the argument's value is queried with. For the arg_value/2 function, the
% allowed minimum number occurences must be 1. For arg_switch/1, the minimum must be 0 and the maximum must
% be 1.
ade_message(ade_min_number_of_occurrences_mismatch(ArgDesc, MinInQuery, QueryFunction)) = Msg :-
    ArgDesc ^ argdesc_min = MMin,
    (   MMin = yes(_ - Props),
        PropMsg = "The required minimum number of occurences has been set equally by the following properties: "
                  ++ human_readable(Props) ++ "."
    ;
        MMin = no,
        PropMsg = "The required minimum number of occurences hasn't been set by any property, that means " ++
                  "it defaults of 0."
    ),
    Msg = "Mismatch in the minimum allowed number of occurrences of the command line argument " ++
          string.string(ArgDesc ^ argdesc_cla) ++ ", which was queried by the " ++ QueryFunction ++
          ". The minimum, as specified in the argument properties, is " ++
          int_to_string(get_min_times(ArgDesc)) ++ " whereas the minimum as required by the argument " ++
          "query function is " ++ int_to_string(MinInQuery) ++ ". " ++ PropMsg.


% The maximum allowed number of occurences of the argument, as specified in the argument properties,
% doesn't match the function which the argument's value is queried with. For the arg_value/2 function, the
% allowed maximum number occurences must be 1. For arg_switch/1, the minimum must be 0, the maximum must
% be 1.
ade_message(ade_max_number_of_occurrences_mismatch(ArgDesc, MaxInQuery, QueryFunction)) = Msg :-
    ArgDesc ^ argdesc_max = MMax,
    (   MMax = yes(_ - Props),
        PropMsg = "The required maximum number of occurences has been set by the following properties: "
                  ++ human_readable(Props) ++ "."
    ;
        MMax = no,
        PropMsg = "The required maximum number of occurences hasn't been set by any property."
    ),
    Msg = "Mismatch in the maximum allowed number of occurrences of the command line argument " ++
          string.string(ArgDesc ^ argdesc_cla) ++ ", which has been queried by the " ++ QueryFunction ++
          ". " ++
          ( if   ArgDesc ^ argdesc_max = yes(MaxInProperties - _)
            then "The maximum, as specified in the argument properties, is " ++ int_to_string(MaxInProperties) ++
                 " whereas the maximum as required by the argument query function is " ++
                 int_to_string(MaxInQuery) ++ ". "
            else "There has been no maximum number of occurrences specified in the properties, but " ++
                 "the argument query function requires a maximum of " ++ int_to_string(MaxInQuery) ++ ". "
          ) ++
          PropMsg.


% An argument that doesn't take a value has been queried with arg_value/2 or arg_values/2, or an argument
% that takes a value has been queried with arg_switch.
ade_message(ade_takes_value_mismatch(ArgDesc, TakesInProperties, TakesInQuery, QueryFunction)) =
    "Mismatch in whether the argument " ++ string.string(ArgDesc ^ argdesc_cla) ++ " takes a value, " ++
    "between the argument properties and the argument query function " ++ QueryFunction ++ "." ++
    ( if   TakesInProperties = yes, TakesInQuery = no
      then " The argument description specifies that the argument takes a value, whereas the " ++
           "argument query function expects an argument which doesn't take a value."
      else if TakesInProperties = no, TakesInQuery = yes
      then " The argument description specifies that the argument doesn't take a value, whereas the " ++
           "argument query function expects an argument which takes a value."
      else unexpected($pred, "ade_takes_value_mismatch argument description error occured where there is " ++
                      "no error.")
    ).

% An argument is queried with a query function which expects a default but the argument has no default,
% or with a query function that expects no default, but the argument has a default.
ade_message(ade_has_default_mismatch(ArgDesc, HasDefault, Expected, QueryFunction)) =
    ( if   HasDefault = yes, Expected = no
      then "The argument " ++ string.string(ArgDesc ^ argdesc_cla) ++ ", which has a default value " ++
           "is queried by the " ++ QueryFunction ++ ", which doesn't allow a default value."
      else "The argument " ++ string.string(ArgDesc ^ argdesc_cla) ++ ", which has no default value, " ++
           "is queried by the " ++ QueryFunction ++ ", which expects a default."
    ).

% Several arguments with the same long name are defined.
ade_message(ade_multiple_long(Long, ArgDescL)) =
    "The same long argument name " ++ quote_qm("--" ++ Long) ++ " is used for several arguments, namely " ++
    human_readable(
        map((func(ArgDesc) = ArgDesc ^ argdesc_cla),
            ArgDescL)).

% Several arguments with the same short name are defined.
ade_message(ade_multiple_short(Short, ArgDescL)) =
    "The same short argument name ""-" ++ string.from_char(Short) ++ """ is used for two arguments, namely " ++
    human_readable(
        map((func(ArgDesc) = ArgDesc ^ argdesc_cla),
            ArgDescL)).

% An argument that isn't a switch has been queried with a query function which requires a switch (arg_switch).
ade_message(ade_not_a_switch(Arg, QueryFunction)) =
    "The argument " ++ quote_qm(cla(Arg)) ++ ", which isn't a switch, has been queried with " ++ QueryFunction ++
    ", which works only with a switch.".

% Usage of prop_default means that there is a value, which must be named with prop_value. The latter hasn't been
% done.
ade_message(ade_missing_valname(Arg, Props)) =
    "The argument " ++ quote_qm(cla(Arg)) ++ " has a value, given by " ++ human_readable(Props) ++
    ". But no name for the value has been specified with prop_value.".



% Assume the argument has an environment variable. Extract and quote its name.

:- func get_argdesc_env(argdesc) = string.

get_argdesc_env(ArgDesc) = Str :-
    MEnv = ArgDesc ^ argdesc_env,
    (   MEnv = yes(Env),
        Str = quote_qm(Env)
    ;   MEnv = no,
        unexpected($pred, "ArgDesc doesn't match Origin")
    ).



% For message generation: Format a list as a human readable string.
:- func human_readable(list(T)) = string.

human_readable([]) = "".

human_readable([X]) = string.string(X).

human_readable(L@[_,_|_]) = R :-
    list.det_split_last(L, Beginning, Last),
    R = string.join_list(", ", map(string.string, Beginning))
        ++ " and " ++ string.string(Last).



%--------------------------------------------------------------------------------------------------------------
% Argument value checkers


:- pred nonneg_integer(int::in, maybe(string)::out) is det.

nonneg_integer(I, Err) :-
    (  if I < 0 then Err = yes("Non-negative integer expected.")
                else Err = no ).



nonneg_integer = make_checker(nonneg_integer).



:- pred positive_integer(int::in, maybe(string)::out) is det.

positive_integer(I, Err) :-
    (  if I =< 0 then Err = yes("Positive integer expected.")
                 else Err = no ).



positive_integer = make_checker(positive_integer).



make_checker(Chk) =
    'new checker'(
        checker_pred(
            (pred(ArgDesc::in, Origin::in, ValIn::in, ValOut::out) is det :-
                Chk(ValIn, MErr),
                (
                    MErr = yes(Msg),
                    throw('new cle_invalid_value'(ArgDesc, ValIn, Origin, yes(Msg)))
                ;
                    MErr = no,
                    ValOut = ValIn
                )))).



%--------------------------------------------------------------------------------------------------------------
% General tools

:- func quote_qm(string) = string.

quote_qm(Str) =
    """" ++ string.from_char_list(quote_qm_cl(string.to_char_list(Str))) ++ """".

:- func quote_qm_cl(list(char)) = list(char).

quote_qm_cl([]) = [].
quote_qm_cl([Ch|Chs]) =
    ( if Ch = '"'
      then ['\\', '"'|quote_qm_cl(Chs)]
      else [Ch|quote_qm_cl(Chs)]
    ).



%--------------------------------------------------------------------------------------------------------------
% Automatic usage info generation.


% Determine the terminal width in columns


terminal_width(Stream, DefWidth, Columns, !IO) :-
    get_text_output_stream_fd(Stream, Fd, !IO),
    terminal_width_fd(Fd, DefWidth, Columns, !IO).



:- pragma foreign_decl("C", "
#include <sys/ioctl.h>
#include <unistd.h>
").

:- pragma foreign_proc("C",
    terminal_width_fd(Fd::in, Default::in, Width::out, IOin::di, IOout::uo),
    [promise_pure, will_not_call_mercury, tabled_for_io], "

  int res;
  struct winsize size;

  res = ioctl (Fd, TIOCGWINSZ, &size);

  if (res == -1)
      Width = Default;
  else
      Width = size.ws_col;

  IOout = IOin;
"
).



:- pragma foreign_proc("C",
    isatty(Fd::in, IsAtty::out, IOin::di, IOout::uo),
    [promise_pure, will_not_call_mercury, tabled_for_io], "

  if (isatty(Fd))
      IsAtty = MR_YES;
  else
      IsAtty = MR_NO;

  IOout = IOin;
"
).



% Get the list of argument descriptions from the compiled arguments.

:- pred get_argdescs(arguments::in, list(argdesc)::out) is det.

get_argdescs(Arguments, ArgDescs) :-
    ArgDescs = map(get_argdesc(Arguments), Arguments ^ arguments_args).



% Get the list of some argument descriptions from the compiled arguments.

get_argdescs(Arguments, ArgL, ArgDescs) :-
    ArgDescs = map(get_argdesc(Arguments), ArgL).



% Print the usage information for all arguments to stdout.

print_usage_info(ArgsProps, !IO) :-
    ArgL = map(fst, ArgsProps),
    print_usage_info(ArgsProps, ArgL, !IO).



% Print the usage information for some arguments to stdout.

print_usage_info(ArgsProps, ArgL, !IO) :-
    map((pred((Arg - PropertyList)::in, ArgDesc::out) is det :-
                condense_properties(Arg, PropertyList, ArgDesc)),
        ArgsProps,
        ArgDescL : list(argdesc)),
    ArgDescL1 = filter((pred(ArgDesc::in) is semidet :-
                         list.member(ArgDesc ^ argdesc_cla, ArgL)),
                       ArgDescL),

    isatty(1, Color, !IO),

    terminal_width(stdout_stream, 80, TerminalWidth, !IO),
    geometry(argval_short(Color), ArgDescL1, TerminalWidth / 10 + 2, WidthShort),
    geometry(argval_long(Color),  ArgDescL1, TerminalWidth / 4,      WidthLong),

    usage_info(ArgDescL1, TerminalWidth - 1, WidthLong, WidthShort, Color, Infos),

    foldl(write_string, Infos, !IO).


% % Generate the usage information for all the arguments in the argument descriptions list. See the prop_description
% % property and get_argdescs, below.

% :- pred usage_info(
%     list(argdesc)::in,          % The argument descriptions
%     int::in,                    % The maximum line width. This should be the width of the terminal on which the
%                                 % usage info is to be displayed.
%     int::in,                    % The maximum width of long argument column
%     int::in,                    % The maximum width of short argument column
%     bool::in,                   % If to use color
%     list(string)::out)          % List of the pieces of usage information for each argument.
% is det.

usage_info(ArgDescL, TerminalWidth, WidthLong, WidthShort, Color, Infos) :-
    map(usage_info_one(TerminalWidth, WidthLong, WidthShort, Color),
        ArgDescL,
        Infos).



usage_info_one(TerminalWidth, WidthLong, WidthShort, Color, ArgDesc, Info) :-
    % Argumentname consisting of the long names which the argument has. Has the form "--aaa/--bbb Valname".
    NameLong = argval_long(Color, ArgDesc),

    % Argumentname consisting of the short names which the argument has. Has the form "-a/-b/-c".
    NameShort = argval_short(Color, ArgDesc),

    % Length of the short argumentname, in codepoints
    LenNameShort = textlen_display(NameShort),

    % The description to lay out. Can be empty.
    Description = get_description(ArgDesc),

    % What to write for the short column. Will be padded to fill the column. When the short name is longer than the
    % column width, then it will stick out of the column.
    ShortPart = NameShort ++ duplicate_char(' ', WidthShort - LenNameShort),

    % Column in the line, where the description begins, if the short/long names don't stick out together.
    DescCol = WidthShort + 2 + WidthLong + 2,

    % Beginning of the argument information. This encompasses the short and long names and the spaces which fill up
    % the gap between the short/long name and the description. The short/long name can pass in the description
    % part. In this case, the start of the description is placed in the next line.
    Info0 = ShortPart ++ "  " ++ NameLong,

    % How many characters wide the name part is (both columns together, possibly sticking out).
    NameCols = textlen_display(Info0),

    layout_text(DescCol, TerminalWidth - DescCol, Description, Layedout),
    (
        if NameCols > DescCol - 2
        then Info = Info0 ++ "\n" ++ duplicate_char(' ', DescCol) ++ Layedout ++ "\n"
        else Info = Info0 ++ duplicate_char(' ', DescCol - NameCols) ++ Layedout ++ "\n"
    ).


%----------------------------------------------------------------------------------------------------

% Parsed line
:- type iline
    ---> iline(int,              % Indentation at the beginning
               list(textel)).    % The words on the line

% Text element
:- type textel
    ---> word(string)           % A word of non-white space characters
    ;    space(int).            % The specified number of space charaters


% Render a line
:- pred render_textels(
    int::in,            % Indentation at the beginning of the line
    list(textel)::in,   % The text elements which make up the line
    string::out         % Line as a string
) is det.

render_textels(Indent, Textels, Str) :-
    Ind = duplicate_char(' ', Indent),
    render_textels_1(Textels, Frags),
    Str = string.join_list("", [Ind|Frags]).

:- pred render_textels_1(
    list(textel)::in,   % The text elements which make up the line
    list(string)::out   % Line fragments in reverse order
) is det.

render_textels_1([], []).

render_textels_1([Textel|Textels], Frags) :-
    (
        Textel = space(Num),
        Frags = [duplicate_char(' ', Num)|Frags1],
        render_textels_1(Textels, Frags1)
    ;
        Textel = word(Str),
        Frags = [Str|Frags1],
        render_textels_1(Textels, Frags1)
    ).



% From a list of text elements, take as many as fit in the rest of the line. The rest of the line is the part
% between the start column and the width of the line.
:- pred take_textels(
    int::in,            % Width of the line
    int::in,            % Start column of the text elements
    list(textel)::in,   % List of text elements to take from
    list(textel)::out,  % Text elements which fit in the space between start column and line end
    list(textel)::out)  % Text elements which don't fit there
is det.

take_textels(_, _, [], [], []).

take_textels(Width, Column, [Textel|Textels], Line, Rest) :-
    (
        Textel = space(Len),
        (
            if
                Column + Len > Width
            then
                % Ignore a space at the end of the line.
                Line = [],
                Rest = Textels
            else
                Line = [Textel|Line1],
                take_textels(Width, Column + Len, Textels, Line1, Rest)
        )
    ;
        Textel = word(Word),
        LenDisplay = textlen_display(Word),
        (
            if Column + LenDisplay > Width
            then
                (
                    if  Column < Width * 2 / 3
                    then
                        % Split up overlong word. This is done when there's still much room on the line.
                        split_by_code_point_display(Word, Width - Column, Str1, Str2),
                        Line = [word(Str1)],
                        Rest = [word(Str2)|Textels]
                    else
                        % No overlong word. End the line here.
                        Line = [],
                        Rest = [Textel|Textels]
               )
            else Line = [Textel|Line1],
                 take_textels(Width, Column + LenDisplay, Textels, Line1, Rest)
        )
    ).


%----------------------------------------------------------------------------------------------------


% This is like string.split_by_code_point/4. The difference is that wide characters are counted as two code points
% when determining where to split the string. Wide characters are used in asian languages. They have a form that
% fits in a square. When the are displayed (on a terminal, for instance), they take the place of two ordinary,
% Roman chracters. This is a example for a wide character: "惠"

:- pred split_by_code_point_display(
    string::in,         % String to split
    int::in,            % Split position, measured in display width
    string::out,        % Substring from 0 to display width
    string::out         % Substring from display width to end
) is det.

split_by_code_point_display(Str, SplitPos, Left, Right) :-
    split_by_code_point_display(Str, SplitPos, 0, 0, CodeUnitIdx),
    split(Str, CodeUnitIdx, Left, Right).


:- pred split_by_code_point_display(
    string::in,                 % String to split
    int::in,                    % Display position where to split
    int::in,                    % The current display position
    int::in, int::out           % The current index in the code units list
) is det.

split_by_code_point_display(Str, SplitPos, DisplayPos, !CodeUnitIdx) :-
    This = !.CodeUnitIdx,
    (
        if
            index_next(Str, !.CodeUnitIdx, !:CodeUnitIdx, Char)
        then
            (
                if uc_iswide(Char)
                then DisplayWidth = 2
                else DisplayWidth = 1
            ),
            (
                if DisplayPos + DisplayWidth = SplitPos
                then true

                % This happens when a wide character overlaps the split position (such that only half of the
                % character would fit in). We split at the position before the character.
                else if DisplayPos + DisplayWidth = SplitPos + 1
                then !:CodeUnitIdx = This

                else split_by_code_point_display(Str, SplitPos, DisplayPos + DisplayWidth, !CodeUnitIdx)
            )
        else
            unexpected($pred, "Wrong index")
    ).

%----------------------------------------------------------------------------------------------------


% Split a list of text elements into lines, such that the lines' width doesn't exceed the specified width.
:- pred textels_to_lines(
    int::in,                    % Line width (excluding the indentation as specified to layout_text and the
                                % indentation of the line inside the text to be layed out)
    list(textel)::in,           % Words
    list(list(textel))::out)    % Lines of text elements
is det.

textels_to_lines(_, [], []).

textels_to_lines(Width, Textels@[_|_], Lines) :-
    take_textels(Width, 0, Textels, Line, Rest0),
    Lines = [Line|Lines1],

    % Remove space at the beginning of the next line.
    (
        Rest0 = [First|Rest1],
        (
            First = space(_),
            Rest2 = Rest1
        ;
            First = word(_),
            Rest2 = Rest0
        )
    ;
        Rest0 = [],
        Rest2 = []
    ),

    textels_to_lines(Width, Rest2, Lines1).



% Render an iline as a list of lines. The iline can be several output lines long. Each gets the indentation of the
% iline at the beginning.
:- pred iline_to_lines(
    int::in,                    % Line width, including the indentation at the beginning
    iline::in,                  % Parsed line with indentation and word list
    list(string)::out)          % Lines of text elements
is det.

iline_to_lines(Width, iline(Indent, TextelL), Lines) :-
    (
        if TextelL = [], Indent = 0
        then
            Lines = [""]
        else
            textels_to_lines(Width - Indent, TextelL, TextelLL),
            map(render_textels(Indent), TextelLL, Lines)
    ).



% Split a parsed line into lines, such that each sublist is split into lines
:- pred ilines_to_lines(
    int::in,                    % Line width
    list(iline)::in,            % List of the ilines of the text to be layed out
    list(string)::out           % Lines
) is det.

ilines_to_lines(Width, Ilines, Lines) :-
    map(iline_to_lines(Width), Ilines, LineLL),
    Lines = condense(LineLL).



% Parse a line
:- pred iline(
    string::in,         % Original line
    iline::out          % Indentation at the beginning of the line and words afters
) is det.

iline(Str, iline(Indent, Textels)) :-
    Str1 = string.to_char_list(Str),
    list.take_while(char.is_whitespace, Str1, Spaces, Rest),
    Indent = length(Spaces),
    iline_1(Rest, Textels).


:- pred iline_1(list(char)::in, list(textel)::out) is det.

iline_1([], []).

iline_1(Chars@[_|_], Textels) :-
    IsSpace = (pred(' '::in) is semidet),

    list.take_while(IsSpace, Chars, Spaces, Rest),
    (
        Spaces = [],
        list.take_while_not(IsSpace, Chars, Word, Rest1),
        Textels = [word(string.from_char_list(Word))|Textels1],
        iline_1(Rest1, Textels1)
    ;
        Spaces = [_|_],
        Textels = [space(length(Spaces))|Textels1],
        iline_1(Rest, Textels1)
    ).



% :- pred layout_text(
%     int::in,            % Start column. This makes a column of spaces at the left of the text.
%     int::in,            % Width of the second column, which will contain the text.
%     string::in,         % The text which is to be layed out
%     string::out)        % The layed out text.
% is det.

layout_text(StartCol, TextWidth, Txt, Str) :-
    LinesTxt = string.split_into_lines(Txt),
    map(iline, LinesTxt, ILines),
    ilines_to_lines(TextWidth, ILines, Lines),
    (
        Lines = [First|Rest],
        map((pred(Line::in, Indented::out) is det :-
                Indented = duplicate_char(' ', StartCol) ++ Line),
            Rest,
            Lines1),
        Str = string.join_list("\n", [First|Lines1])
    ;
        Lines = [],
        Str = ""
    ).



print_on_terminal(Text, !IO) :-
    print_on_terminal(io.stdout_stream, Text, !IO).


print_on_terminal(Stream, Text, !IO) :-
    terminal_width(Stream, 80, Width, !IO),
    layout_text(0, Width, Text, Str),
    io.write_string(Stream, Str, !IO),
    io.nl(!IO).




:- pred get_text_input_stream_fd(io.text_input_stream::in, int::out,
      io::di, io::uo) is det.

:- pragma foreign_proc("C",
     get_text_input_stream_fd(Stream::in, FD::out, _IO0::di, _IO::uo),
     [will_not_call_mercury, promise_pure, tabled_for_io],
"
     FD = fileno(MR_file(*MR_unwrap_input_stream(Stream)));
").

:- pred get_text_output_stream_fd(io.text_output_stream::in, int::out,
       io::di, io::uo) is det.

:- pragma foreign_proc("C",
     get_text_output_stream_fd(Stream::in, FD::out, _IO0::di, _IO::uo),
     [will_not_call_mercury, promise_pure, tabled_for_io],
"
     FD = fileno(MR_file(*MR_unwrap_output_stream(Stream)));
").



:- pred geometry(
    (func(argdesc) = string)::in,
    list(argdesc)::in,                  % The descriptions of the arguments which are to be printed
    int::in,                            % Maximum width for the argument names
    int::out)                           % Calculated with for the argument names, <= max
is det.

geometry(Func, ArgDescs, MaxWidth, Width) :-
    map((pred(ArgDesc::in, Width0::out) is det :-
            Width0 = textlen_display(Func(ArgDesc))),
        ArgDescs,
        Widths),
    foldl(max, Widths, 0, Max),
    Width = min(Max, MaxWidth).



print_subcommands_info(SubcList, !IO) :-
    terminal_width(stdout_stream, 80, Width, !IO),
    isatty(1, Color, !IO),

    % Determine maxium lenght of action names
    foldl(
        pred(Act - _::in, !.W::in, !:W::out) is det :-
            !:W = max(!.W, string.length(Act)),
        SubcList,
        0,
        ActWidth0),
    ActWidth = ActWidth0 + 2,

    foldl(
        (pred((Action - Txt)::in, !.IO1::di, !:IO1::uo) is det :-
            io.write_string( if Color = yes then "[00;32m" else "" , !IO1),
            io.write_string(string.pad_right(Action, ' ', ActWidth), !IO1),
            io.write_string( if Color = yes then "[00m" else "" , !IO1),
            layout_text(ActWidth, Width - ActWidth, Txt, Str),
            io.write_string(Str, !IO1),
            io.nl(!IO1)
        ),
        SubcList,
        !IO).



% Create a name from the short names of an argument. If it had three short argument names, say 'q', 'v' and 'x',
% then the name would be "-q/-v/-x". The name of the value of an argument is normally placed after the long
% argument name (see argval_long). If there are no long argument names, the short name carries the value name
% instead. So, for a value name of "Val", the name created by argval_short would be "-q/-v/-x Val".

:- func argval_short(bool, argdesc) = string.

argval_short(Color, ArgDesc) =
    join_list("/",
        map(
            (func(Short) =
                ( if Color = yes then "[00;32m-" ++ string.from_char(Short) ++ "[00m"
                                 else "-" ++ string.from_char(Short)
                )
            ),
            ArgDesc ^ argdesc_short))
    ++ ( if   ArgDesc ^ argdesc_long = [],
              ArgDesc ^ argdesc_valname = yes(Valname)
         then " " ++ ( if Color = yes then "[00;32m" ++ Valname ++ "[00m"
                                      else Valname )
         else ""
       ).


% Create a name from the long names of an argument. If it had two long argument names, say "argu" and "foo", then
% the name would be "--argu/--foo". The name of the value of an argument is normally placed after that long
% argument name. If there are no long argument names, the short name carries the value name instead. But if there
% are any long argument names, and the argument takes a value "Val", the name of the argument value will be placed
% after the joint long argument names, like this: "--argu/--foo Val".

:- func argval_long(bool, argdesc) = string.

argval_long(Color, ArgDesc) =
    join_list("/",
        map(
            (func(Long) =
                ( if Color = yes then "[00;32m--" ++ Long ++ "[00m"
                                 else "--" ++ Long
                )
            ),
            ArgDesc ^ argdesc_long))
    ++ ( if   ArgDesc ^ argdesc_long \= [],
              ArgDesc ^ argdesc_valname = yes(Valname)
         then " " ++ ( if Color = yes then "[00;32m" ++ Valname ++ "[00m"
                                      else Valname
                     )
         else ""
       ).



:- func get_description(argdesc) = string.

get_description(ArgDesc) = Desc :-
    (
        if   ArgDesc ^ argdesc_description = yes(Desc0)
        then Desc = Desc0
        else Desc = ""
    ).



% Determine the length of a string when it is printed on a display (terminal). This takes into account that the
% characters of asian languages take the place of two simple chracters. In addition, ANSI ESC-sequences (which are
% only used for color) aren't counted.

:- func textlen_display(string) = int.

textlen_display(Txt) =
    textlen_display_cl(string.to_char_list(Txt)).


:- func textlen_display_cl(list(char)) = int.

textlen_display_cl([]) = 0.

textlen_display_cl([C|Rest]) =
    ( if
        C = ''
      then
          textlen_display_cl(drop_while(
              pred(D::in) is semidet :- D \= 'm',
              Rest)) - 1
      else if
          uc_iswide(C)
      then
          textlen_display_cl(Rest) + 2
      else
          textlen_display_cl(Rest) + 1
    ).


% Determine the number of unicode code points in a string. ANSI ESC-sequences (which are
% only used for color) aren't counted.

:- func textlen_code_points(string) = int.

textlen_code_points(Txt) =
    textlen_code_points_cl(string.to_char_list(Txt)).


:- func textlen_code_points_cl(list(char)) = int.

textlen_code_points_cl([]) = 0.

textlen_code_points_cl([C|Rest]) =
    ( if
        C = ''
      then
          textlen_code_points_cl(drop_while(
              pred(D::in) is semidet :- D \= 'm',
              Rest)) - 1
      else if
          uc_iswide(C)
      then
          textlen_code_points_cl(Rest) + 2
      else
          textlen_code_points_cl(Rest) + 1
    ).



sort_argdescl(ArgdescLIn, ArgdescLOut) :-
    list.sort(
        (pred(A::in, B::in, Ord::out) is det :-
            compare(Ord, fstlong(snd(A)), fstlong(snd(B)))),
        ArgdescLIn,
        ArgdescLOut).


:- func fstlong(list(property)) = string.

fstlong(PropL) = Str :-
    (
        if   list.find_first_match(
               (pred(Pr::in) is semidet :- Pr = prop_long(_)),
               PropL,
               Prop)
        then ( if Prop = prop_long(Long) then Str = Long
                                         else unexpected($pred, "Wrong property found.")
             )
        else Str = ""
    ).



print_usage_info_with_subcommands(Subcommands, ArgsDescL, !IO) :-
    io.write_string("The subcommands are:\n\n", !IO),
    print_subcommands_info(Subcommands, !IO),

    io.write_string("\nThe arguments are:\n\n", !IO),
    print_usage_info(ArgsDescL, !IO).



print_usage_info_with_subcommands(Subcommands, ArgsDescL, ArgL, !IO) :-
    io.write_string("The subcommands are:\n\n", !IO),
    print_subcommands_info(Subcommands, !IO),

    io.write_string("\nThe arguments are:\n\n", !IO),
    print_usage_info(ArgsDescL, ArgL, !IO).



%--------------------------------------------------------------------------------------------------------------
% Abbreviation of long-form ("--abc...") command line arguments.


% In GNU style command line arguments, a long argument ("--arg...") can be abbreviated, as long as it still is
% unique among all arguments.
%
% For instance: If there are the two arguments "--target-directory" and "--target", then the list of abbreviations
% for  "--target-directory" is:
%
% "target-"
% "target-d"
% "target-di"
% "target-dir"
% "target-dire"
% "target-direc"
% "target-direct"
% "target-directo"
% "target-director"
% "target-directory"


% This predicate determines all the unique abbreviations of a command line argument.

:- pred shortcuts(
    string::in,                 % Full long argument name
    list(string)::in,           % Other full long argument names
    list(string)::out)          % List of all shortcuts of the argument name (including itself)
is det.

shortcuts(Full, Others, Shortcuts) :-
    shortcuts1(Full, Others, Shortcuts0),
    ( Shortcuts0 = [],    Shortcuts = [Full]
    ; Shortcuts0 = [_|_], Shortcuts = Shortcuts0).


:- pred shortcuts1(
    string::in,                 % Full long argument name
    list(string)::in,           % Other full long argument names
    list(string)::out)          % List of all shortcuts of the argument name (possibly including itself)
is det.

shortcuts1(Full, Others, Shortcuts) :-
    solutions(
        shortcut(
            to_char_list(Full),
            map(to_char_list, Others)),
        Shortcuts0),
    Shortcuts = map(from_char_list, Shortcuts0).


:- pred shortcut(
    list(char)::in,
    list(list(char))::in,
    list(char)::out)
is nondet.

shortcut(Full, Others, Shortcut) :-
    list.append(Shortcut, _, Full),
    not(( member(Other, Others),
          list.append(Shortcut, _, Other) )).




%----------------------------------------------------------------------------------------------------
% Framework for a program with subcommands


print_extended_usage_info(HeaderFkt, Subcommands, ArgDescs, !IO) :-
    io.progname_base("", Progname, !IO),
    print_on_terminal(HeaderFkt(Progname), !IO),
    io.nl(!IO),
    sort_subcommands(
       Subcommands ++ ["help" - "Print this usage information."],
       SubcommandsSorted),
    sort_argdescl(ArgDescs, ArgDescs1),
    print_usage_info_with_subcommands(SubcommandsSorted, ArgDescs1, !IO).



program(Subcommands, ArgDescs, HeaderFkt, Main, !IO) :-
    program_extended(
        Subcommands,
        ArgDescs,
        print_extended_usage_info(HeaderFkt, Subcommands, ArgDescs),
        Main,
        !IO).


program_extended(Subcommands, ArgDescs, ExtendedUsageInfo, Main, !IO) :-
    io.command_line_arguments(CLPs, !IO),
    io.progname_base("", Progname, !IO),

    io.environment.get_environment_var_map(Env, !IO),
    clparse_full_1(parse_gnu, ArgDescs, CLPs, Env, Arguments, NonArguments0, ArgDescs1),

    (
      if
          NonArguments0 = [ Chosen | NonArguments ]

      then
          (
              if   Chosen = "help"
              then
                   ExtendedUsageInfo(!IO)
              else
                   verify_args(ArgDescs1, Arguments, Arguments1),
                   (
                       chosen_subcommand(Chosen, map(fst, Subcommands), Full),
                       Main(Full, Arguments1, NonArguments, !IO)
                   )
          )

      else throw(cle_no_subcommand(
               ( if   Progname = ""
                 then "No subcommand specified. Call the program with ""help"" for help."
                 else "No subcommand specified. Call """ ++ Progname ++ " help"" for help."
               )))
    ).



% Sort subcommands list by subcommand name

:- pred sort_subcommands(assoc_list(string, string)::in, assoc_list(string, string)::out).

sort_subcommands(In, Out) :-
    list.sort(
        (pred((Cmd1 - _)::in, (Cmd2 - _)::in, Cmp::out) is det :-
            compare(Cmp, Cmd1, Cmd2)),
        In,
        Out).



catch_cle_ade(CleEC, AdeEC, Pred, !IO) :-
    (
        try [io(!IO)]
            Pred(!IO)
        then
            true
        catch (CLE : command_line_error) ->
            print_on_terminal(stderr_stream, "Command Line Error: " ++ cle_message(CLE) ++ "\n\n", !IO),
            throw(shutdown(CleEC))
        catch (ADE : arg_desc_error) ->
            print_on_terminal(stderr_stream, "Bug found: " ++ ade_message(ADE) ++ "\n\n", !IO),
            throw(shutdown(AdeEC))
    ).



%----------------------------------------------------------------------------------------------------
% Convert any type to string, with "argdesc" and "arguments" components abbreviated to "<<argdesc>>" or
% "<<arguments>>" respectively. This is meant to provide more readable output, by hiding parts that aren't of
% interest.

shortened(T, Str) :-
    shortened_1(univ.univ(T), Str).

shortened(T) = Str :-
    shortened(T, Str).



:- pred shortened_1(univ::in, string::out) is det.

shortened_1(Univ, Str) :-
    deconstruct.deconstruct(univ_value(Univ), deconstruct.canonicalize, Functor, _Arity, Arguments),
    TypeName = type_name(univ.univ_type(Univ)),
    (
        if
            TypeName = "colipa.argdesc"
        then
            Str = "<<argdesc>>"

        else if
            TypeName = "colipa.arguments"
        then
            Str = "<<arguments>>"

        else if
            Functor = "[|]",
            Arguments = []
        then
            Str = "[]"

        else if
            Functor = "[|]",
            Arguments = [A, B]
        then
            Str = "[" ++ shortened_1(A) ++ shortened_list(B) ++ "]"

        else if
            Arguments = []
        then
            Str = Functor
        else
            Str = Functor ++ "(" ++ join_list(", ", map(shortened_1, Arguments)) ++ ")"
    ).


:- func shortened_1(univ) = string.

shortened_1(Univ) = X :- shortened_1(Univ, X).


:- func shortened_list(univ) = string.

shortened_list(Univ) = Str :-
    deconstruct.deconstruct(univ_value(Univ), deconstruct.canonicalize, Functor, _Arity, Arguments),
    (
        if  Arguments = [A, B],
            Functor = "[|]"
        then
            Str = ", " ++ shortened_1(A) ++ shortened_list(B)
        else if
            Functor = "[|]",
            ( Arguments = [] ; Arguments = [_,_,_|_] )
        then
            unexpected($pred, "Invalid number of arguments for functor [|]")
        else if
            Functor = "[]"
        then
            Str = ""
        else
            shortened_1(Univ, Str)
    ).




%----------------------------------------------------------------------------------------------------

catch_cle_ade_extra(CleEC, AdeEC, Pred, !IO) :-
    (
        try [io(!IO)]
            Pred(!IO)
        then
            true
        catch (CLE : command_line_error) ->
            shortened(CLE, CLEDesc),
            print_on_terminal(
                stderr_stream,
                "Caught command line error in catch_cle_ade_extra/5:\n" ++ CLEDesc ++
                "\n\nCommand Line Error: " ++ cle_message(CLE) ++ "\n\n",
                !IO),
            throw(shutdown(CleEC))
        catch (ADE : arg_desc_error) ->
            shortened(ADE, ADEDesc),
            print_on_terminal(
                stderr_stream,
                "Caught argument description error in catch_cle_ade_extra/5:\n" ++ ADEDesc ++
                "\n\nBug found: " ++ ade_message(ADE) ++ "\n\n",
                !IO),
            throw(shutdown(AdeEC))
    ).


%----------------------------------------------------------------------------------------------------
% Placing additional constraints on command line arguments

% Exactly one of the arguments must be specified on the command line.
args_one(Arguments, CLAs, MaybeMsgGen) :-
    args_helper(
        Arguments,
        CLAs,
        (pred(Int::in) is semidet :- Int \= 1),
        (func(Arguments1, Args1, Msg1) = cle_args_one(Arguments1, Args1, Msg1)),
        MaybeMsgGen,
        args_one_defmsg).


args_one_defmsg(ArgDescL) =
    "Of the following command line arguments, exactly one must be specified: " ++
    argsname(ArgDescL) ++ ".".


% At least one of the arguments must be specified on the command line.
args_at_least_one(Arguments, CLAs, MaybeMsgGen) :-
    args_helper(
        Arguments,
        CLAs,
        (pred(Int::in) is semidet :- Int < 1),
        (func(Arguments1, Args1, Msg1) = cle_args_at_least_one(Arguments1, Args1, Msg1)),
        MaybeMsgGen,
        args_at_least_one_defmsg).


args_at_least_one_defmsg(ArgDescL) =
    "Of the following command line arguments, at least one must be specified: " ++
    argsname(ArgDescL) ++ ".".


% At most one of the arguments must be specified on the command line.
args_at_most_one(Arguments, CLAs, MaybeMsgGen) :-
    args_helper(
        Arguments,
        CLAs,
        (pred(Int::in) is semidet :- Int > 1),
        (func(Arguments1, Args1, Msg1) = cle_args_at_most_one(Arguments1, Args1, Msg1)),
        MaybeMsgGen,
        args_at_most_one_defmsg).


args_at_most_one_defmsg(ArgDescL) =
    "Of the following command line arguments, at most one must be specified: " ++
    argsname(ArgDescL) ++ ".".



% Count the occurences of the specified arguments. Each argument that occurs on the command line, counts as 1 -
% even if it occurs several times. The result is the number of arguments from the list, which occur on the command
% line.

:- func count_occuring(arguments, list(cla)) = int.

count_occuring(_, []) = 0.

count_occuring(Arguments, [Arg|ArgList]) = Num :-
    Num1 = count_occuring(Arguments,  ArgList),
    Times = get_times(Arguments, Arg),
    (
        if Times > 0
        then Num = Num1 + 1
        else Num = Num1
    ).



:- pred args_helper(
    arguments,                                                  % Arguments
    list(cla),                                                  % Args
    pred(int),                                                  % Test
    (func(arguments, list(cla), string) = command_line_error),  % ErrGen
    maybe(func(list(argdesc)) = string),                        % MaybeMsgGen
    func(list(argdesc)) = string                                % MsgGenDef
).

:- mode args_helper(
    in,                                                         % Arguments
    in,                                                         % Args
    in(pred(in) is semidet),                                    % Test
    in(func(in, in, in) = out is det),                          % ErrGen
    in(maybe(func(in) = out is det)),                           % MaybeMsgGen
    in(func(in) = out is det)                                   % MsgGenDef
) is det.

args_helper(Arguments, CLAs, Test, ErrGen, MaybeMsgGen, MsgGenDef) :-
    Occ = count_occuring(Arguments, CLAs),
    get_argdescs(Arguments, CLAs, ArgDescs),
    (
        if   MaybeMsgGen = yes(MsgGen0)
        then MsgGen = MsgGen0
        else MsgGen = MsgGenDef
    ),
    (
        if Test(Occ)
        then throw(ErrGen(Arguments, CLAs, MsgGen(ArgDescs) : string))
        else true
    ).



%----------------------------------------------------------------------------------------------------

% main(!IO) :-
%     Text = "       This  is some   text with         weird spaces       " ++
%            "and an indentation    at the beginning of the line.",
%     print_on_terminal(Text, !IO).
%     % iline(Text, Iline@iline(_Ind, Textels)),
%     % iline_to_lines(40, Iline, Lines),
%     % io.write_string("Texels=\n", !IO),
%     % io.print(Textels, !IO), io.nl(!IO),

%     % io.write_string("Iline=\n", !IO),
%     % write_list(Lines, "\n", io.print, !IO).

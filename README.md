# Colipa - A Command Line Parser for Mercury

Colipa is a command line parser for the programming language [Mercury](https://mercurylang.org).

## Version

This is Colipa version 2.6.4, as of 2024-10-28. The author is Volker Wysk (<post@volker-wysk.de>).

## Design goals

* Make it as easy to use as possible
* Support the programmer and user with expressive error messages
* Be (mostly) feature complete

## Features

* Supports GNU style command line arguments.
* Command line arguments are defined by a list of properties. Those tell Colipa things like the long and short
  names of the argument, if it takes a value, a possible default value, a textual description of the argument etc.
* Each argument can have multiple long and short names.
* With argument query functions, you extract the data you want, with the type you want, from the command line.
  Those functions compare what you're looking for with what is specified in the argument's properties. In case of a
  mismatch, you get a particular exception which tells you what went wrong.
* Arguments can have default values (but don't have to).
* Argument values can be taken from environment variables. In descending priority, an argument's value can be given
  on the command line, in an environment variable or in a default.
* You get your command line values in the right types. Parsing is done transparently.
* An argument can have a checker. That's a function which places additional requirements on an argument value,
  apart from the well-formedness which is needed to parse it.
* Expressive error messages for both errors caused by the user of the program, and erros (bugs) caused by the
  developer who uses Colipa. For instance, if you have two properties which conflict with each other, Colipa tells
  you which argument and which properties.
* Clear usage information, which lists the arguments and their use, can be automatically generated from the
  arguments' properties.
* Text can be formatted, such that it fits the terminal's width and is wrapped at the right side, without breaking
  words in two. This is used for usage information, but can be used for other text as well.
* It should be easy to extend - argument types, checkers, new styles of parsers, new properties...
* There are provisions for dealing with subcommands.

## Requirements

You need a working installation of the Melbourne Mercury Compiler. I've developed and tested it with a recent
ROTD version. If you're using a stable version, there's a small, theoretical possibility that it won't compile.
Please contact me if you're facing any problems.

In Colipa version 2.6.4, a dependency has been introduced. I need the glib library for determining if a Unicode
character is a "wide" character, which takes the place of two ordinary characters in a terminal. This is the case
in asian languages, for instance: "最". This is needed for the correct layout of text, when it contains such
characters. The concerned predicates are layout_text and print_on_terminal.

In Linux, and probably MacOS, this is no problem. In Windows, you likely need to install the glib library. See
here: [https://docs.gtk.org/glib/](https://docs.gtk.org/glib/). I can't test this, since I use Linux exclusively.
Please contact me when you're trying to install it on Windows.

You can also use the version 2.6.3, which doesn't have this dependency (but displays wide characters faulty).

## Introduction

An introduction on how to use Colipa is 
[here in the wiki](https://gitlab.com/volkerwysk/colipa/-/wikis/Introduction-to-Colipa).

## Compilation

When you want to compile a program, which uses Colipa version 2.6.4 and higher, you need to include the glib
library in your `mmc` command line. For versions up to 2.6.3, this isn't needed. Under Linux:

```
$(pkg-config --libs glib-2.0)
--cflags "$(pkg-config --cflags glib-2.0)"
```

If you want to compile Colipa, but don't want to install it, you can use the `make.sh` script. You can also
compile the test programs. Specity the name of the program in the `make.sh` command line.

## Installation

Colipa doesn't require installation. It's just the `colipa.m` file and if can be incorporated in your project
directly. However, it can be installed with the `install.sh` script. 

## Copyright and Warranty

Copyright 2022-2024 Volker Wysk <<post@volker-wysk.de>>

Colipa is distributed under the terms of the GNU Lesser General Public License, Version 3 or any later version.
See the file COPYING.LESSER for details.

Colipa is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

Colipa is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License along with Colipa. If not, see
<https://www.gnu.org/licenses/>.

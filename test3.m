:- module test3.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.

:- implementation.
:- import_module list, int, string, char, bool, assoc_list, pair, maybe.


:- type defaultwrap ---> some [T] defaultwrap(T). 

:- func l = list(defaultwrap).
l = ['new defaultwrap'(1), 'new defaultwrap'(yes), 'new defaultwrap'("hallo")].

main(!IO) :-
    io.write_string(string(l : list(defaultwrap)), !IO),
    io.nl(!IO),

    L1 = list.sort(l),
    io.write_string(string(L1 : list(defaultwrap)), !IO),
    io.nl(!IO).

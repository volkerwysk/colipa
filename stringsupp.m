%====================================================================================================
% stringsupp - Supplementary library for the standard string library.
%====================================================================================================

:- module stringsupp.

:- interface.
:- import_module string, list.


% Nondeterministically generate all the prefixes of the string. This includes the empty prefix and the full
% string. This is like the prefix/2 predicate from the standard string library, but supports a mode (generate the
% prefixes) that the standard library doesn't support.

:- pred prefix(string, string).
:- mode prefix(out, in) is multi.


% Deterministically generate all the prefixes of the string. This includes the empty prefix and the full string.

:- pred prefixes(string, list(string)).
:- mode prefixes(in, out) is det.

:- func prefixes(string) = list(string).



%====================================================================================================
:- implementation.
:- import_module int, solutions.


% Nondeterministically generate the numbers from 0 to Max, inclusive. One could use "list.member(Nr, 0 ..
% length(Str))", but upto should be somewhat more efficient.

:- pred upto(int::in, int::out) is multi.

upto(Max, Int) :-
    upto(0, Max, Int).


:- pred upto(int::in, int::in, int::out) is multi.

upto(Nr, Max, Int) :-
    (
        if Nr = Max
        then Int = Nr
        else
            (  Int = Nr
            ;  upto(Nr + 1, Max, Int)
            )
    ).



prefix(Prefix, Str) :-
    upto(length(Str), Nr),
    split_by_code_point(Str, Nr, Prefix, _).


prefixes(Str, Prefixes) :-
    prefixes_1(0, "", Str, Prefixes0),
    Prefixes = ["" | Prefixes0].


:- pred prefixes_1(int::in, string::in, string::in, list(string)::out) is det.

prefixes_1(Index, Pref, Str, Prefixes) :-
    (
        if
            index_next(Str, Index, NextIndex, Char)
        then
            Pref1 = Pref ++ string.from_char(Char),
            Prefixes = [ Pref1 | Rest ],
            prefixes_1(NextIndex, Pref1, Str, Rest)
        else
            Prefixes = []
    ).



prefixes(Str) = L :-
    prefixes(Str, L).


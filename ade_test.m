% This is a test program for the generation of argument description errors (type arg_desc_error).

%--------------------------------------------------------------------------------------------------------------
% Copyright 2022-2024 Volker Wysk <post@volker-wysk.de>
% This file is distributed under the terms of the GNU Lesser General Public License, Version 3 or later. See
% the file COPYING.LESSER for details.
%--------------------------------------------------------------------------------------------------------------

:- module ade_test.

:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is cc_multi.


:- implementation.
:- import_module list, int, maybe, string, char, bool, map, pair, assoc_list, float, require.
:- import_module colipa.
:- import_module deconstruct, type_desc, univ.


:- func arg_valname = pair(cla, list(property)).
arg_valname =
        cla("valname") -
        [ prop_value("MyVal"),
          prop_long("valname")
        ].

:- func arg_double_valname = pair(cla, list(property)).
arg_double_valname =
        cla("arg-double-valname") -
        [ prop_value("foo"),
          prop_value("bar"),
          prop_value("baz"),
          prop_long("arg_double_valname")
        ].

:- func arg_double_valname_2 = pair(cla, list(property)).
arg_double_valname_2 =
        cla("arg-double-valname-2") -
        [ prop_value("foo"),
          prop_switch_value("bar"),
          prop_long("arg_double_valname_2")
        ].

:- func arg_switch_one = pair(cla, list(property)).
arg_switch_one =
        cla("arg-switch-one") -
        [ prop_short('s'),
          prop_switch,
          prop_one
        ].

:- func arg_no_name = pair(cla, list(property)).
arg_no_name =
        cla("arg-no-name") -
        [ prop_switch_value("Sw")
        ].

:- func arg_missing_value_name = pair(cla, list(property)).
arg_missing_value_name =
        cla("arg-missing-value-name") -
        [ prop_long("miss"),
          'new prop_default'(123)
        ].

:- func arg_duplicate_long_1 = pair(cla, list(property)).
arg_duplicate_long_1 =
        cla("arg-duplicate-long-1") -
        [ prop_long("dup"),
          prop_long("foo"),
          prop_switch
        ].

:- func arg_duplicate_long_2 = pair(cla, list(property)).
arg_duplicate_long_2 =
        cla("arg-duplicate-long-2") -
        [ prop_long("bar"),
          prop_long("dup"),
          prop_switch
        ].


main(!IO) :-
    trigger_ade("Argument defined twice",
        [arg_valname, arg_valname],
        !IO),

    trigger_ade("Conflicting argument value names",
        [arg_double_valname],
        !IO),

    trigger_ade("Conflicting argument value names 2",
        [arg_double_valname_2],
        !IO),

    trigger_ade("Conflicting occurence times",
        [arg_switch_one],
        !IO),

    trigger_ade("Argument without name",
        [arg_no_name],
        !IO),

    trigger_ade("Argument with default value but no value name",
        [arg_missing_value_name],
        !IO),

    trigger_ade("Same argument name used twice",
        [arg_duplicate_long_1, arg_duplicate_long_2],
        !IO).


% Trigger the arg_desc_error which is included in the provided arguments description.

:- pred trigger_ade(
    string::in,
    assoc_list(cla, list(property))::in,
    io::di, io::uo
) is cc_multi.

trigger_ade(TestName, ArgDescL, !IO) :-
    io.write_string("\u001b[34mTest: " ++ TestName ++ "\u001b[0m\n", !IO),
    (
        try [io(!IO)]
            colipa(ArgDescL, _, _, !IO)
        then
            io.write_string("Test passed. No error.\n", !IO)
        catch (CLE : command_line_error) ->
            print_on_terminal(stdout_stream, "Command Line Error: " ++ cle_message(CLE) ++ "\n\n", !IO)
        catch (ADE : arg_desc_error) ->
            print_on_terminal(stdout_stream,
                "Argument Description Error:\n" ++
                shortened(ADE) ++ "\n\n" ++
                ade_message(ADE) ++ "\n\n",
                !IO)
    ).


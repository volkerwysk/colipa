<h1 id="colipa---a-command-line-parser-for-mercury">Colipa - A Command Line Parser for Mercury</h1>

<p>Colipa is a command line parser for the programming language <a href="https://mercurylang.org">Mercury</a>.</p>

<h2 id="version">Version</h2>

<p>This is Colipa version 2.6.4, as of 2024-10-28. The author is Volker Wysk (<a href="mailto:post@volker-wysk.de">post@volker-wysk.de</a>).</p>

<h2 id="design-goals">Design goals</h2>

<ul>
  <li>Make it as easy to use as possible</li>
  <li>Support the programmer and user with expressive error messages</li>
  <li>Be (mostly) feature complete</li>
</ul>

<h2 id="features">Features</h2>

<ul>
  <li>Supports GNU style command line arguments.</li>
  <li>Command line arguments are defined by a list of properties. Those tell Colipa things like the long and short
names of the argument, if it takes a value, a possible default value, a textual description of the argument etc.</li>
  <li>Each argument can have multiple long and short names.</li>
  <li>With argument query functions, you extract the data you want, with the type you want, from the command line.
Those functions compare what you’re looking for with what is specified in the argument’s properties. In case of a
mismatch, you get a particular exception which tells you what went wrong.</li>
  <li>Arguments can have default values (but don’t have to).</li>
  <li>Argument values can be taken from environment variables. In descending priority, an argument’s value can be given
on the command line, in an environment variable or in a default.</li>
  <li>You get your command line values in the right types. Parsing is done transparently.</li>
  <li>An argument can have a checker. That’s a function which places additional requirements on an argument value,
apart from the well-formedness which is needed to parse it.</li>
  <li>Expressive error messages for both errors caused by the user of the program, and erros (bugs) caused by the
developer who uses Colipa. For instance, if you have two properties which conflict with each other, Colipa tells
you which argument and which properties.</li>
  <li>Clear usage information, which lists the arguments and their use, can be automatically generated from the
arguments’ properties.</li>
  <li>Text can be formatted, such that it fits the terminal’s width and is wrapped at the right side, without breaking
words in two. This is used for usage information, but can be used for other text as well.</li>
  <li>It should be easy to extend - argument types, checkers, new styles of parsers, new properties…</li>
  <li>There are provisions for dealing with subcommands.</li>
</ul>

<h2 id="requirements">Requirements</h2>

<p>You need a working installation of the Melbourne Mercury Compiler. I’ve developed and tested it with a recent
ROTD version. If you’re using a stable version, there’s a small, theoretical possibility that it won’t compile.
Please contact me if you’re facing any problems.</p>

<p>In Colipa version 2.6.4, a dependency has been introduced. I need the glib library for determining if a Unicode
character is a “wide” character, which takes the place of two ordinary characters in a terminal. This is the case
in asian languages, for instance: “最”. This is needed for the correct layout of text, when it contains such
characters. The concerned predicates are layout_text and print_on_terminal.</p>

<p>In Linux, and probably MacOS, this is no problem. In Windows, you likely need to install the glib library. See
here: <a href="https://docs.gtk.org/glib/">https://docs.gtk.org/glib/</a>. I can’t test this, since I use Linux exclusively.
Please contact me when you’re trying to install it on Windows.</p>

<p>You can also use the version 2.6.3, which doesn’t have this dependency (but displays wide characters faulty).</p>

<h2 id="introduction">Introduction</h2>

<p>An introduction on how to use Colipa is 
<a href="https://gitlab.com/volkerwysk/colipa/-/wikis/Introduction-to-Colipa">here in the wiki</a>.</p>

<h2 id="compilation">Compilation</h2>

<p>When you want to compile a program, which uses Colipa version 2.6.4 and higher, you need to include the glib
library in your <code>mmc</code> command line. For versions up to 2.6.3, this isn’t needed. Under Linux:</p>

<p><code>
$(pkg-config --libs glib-2.0)
--cflags "$(pkg-config --cflags glib-2.0)"
</code></p>

<p>If you want to compile Colipa, but don’t want to install it, you can use the <code>make.sh</code> script. You can also
compile the test programs. Specity the name of the program in the <code>make.sh</code> command line.</p>

<h2 id="installation">Installation</h2>

<p>Colipa doesn’t require installation. It’s just the <code>colipa.m</code> file and if can be incorporated in your project
directly. However, it can be installed with the <code>install.sh</code> script.</p>

<h2 id="copyright-and-warranty">Copyright and Warranty</h2>

<p>Copyright 2022-2024 Volker Wysk «post@volker-wysk.de»</p>

<p>Colipa is distributed under the terms of the GNU Lesser General Public License, Version 3 or any later version.
See the file COPYING.LESSER for details.</p>

<p>Colipa is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.</p>

<p>Colipa is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
more details.</p>

<p>You should have received a copy of the GNU Lesser General Public License along with Colipa. If not, see
<a href="https://www.gnu.org/licenses/">https://www.gnu.org/licenses/</a>.</p>

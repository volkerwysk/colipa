%====================================================================================================
% unicode - Some Unicode Support 
%====================================================================================================

:- module unicode.

:- interface.
:- import_module char, io.

% Unicode-aware character conversion predicates
:- pred uc_toupper(char::in, char::out) is det.
:- pred uc_tolower(char::in, char::out) is det.

% Unicode-aware character classification predicates
:- pred uc_isalnum(char::in) is semidet.
:- pred uc_isupper(char::in) is semidet.
:- pred uc_islower(char::in) is semidet.
:- pred uc_isalpha(char::in) is semidet.
:- pred uc_iswide(char::in) is semidet.

% Unicode-aware string collation functions/predicates
:- pred uc_collate(string::in, string::in, builtin.comparison_result::out) is det.
:- func uc_collate(string, string) = builtin.comparison_result is det.

% Setting the locale. This must be called at the beginning of the program to make the Unicode aware functions and
% predicates take effect.
:- pred setlocale(io::di, io::uo) is det.


%====================================================================================================
:- implementation.
:- import_module bool, int.

% See https://developer.gnome.org/glib/stable/

:- pragma foreign_decl("C", "
#include <glib.h>
#include <locale.h>
").


:- pragma foreign_proc("C",
    uc_toupper(Ch::in, Upch::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Upch = g_unichar_toupper(Ch);
").



:- pragma foreign_proc("C",
    uc_tolower(Ch::in, Upch::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Upch = g_unichar_tolower(Ch);
").


:- pred uc_isalnum0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    uc_isalnum0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_isalnum(Ch);
").


uc_isalnum(Ch) :-
    uc_isalnum0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred uc_isupper0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    uc_isupper0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_isupper(Ch);
").


uc_isupper(Ch) :-
    uc_isupper0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred uc_islower0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    uc_islower0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_islower(Ch);
").


uc_islower(Ch) :-
    uc_islower0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred uc_isalpha0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    uc_isalpha0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_isalpha(Ch);
").


uc_isalpha(Ch) :-
    uc_isalpha0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred uc_iswide0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    uc_iswide0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
    Res = g_unichar_iswide(Ch);
").


uc_iswide(Ch) :-
    uc_iswide0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred c_uc_collate(string::in, string::in, int::out) is det.

:- pragma foreign_proc("C",
    c_uc_collate(Str1::in, Str2::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
    Res = g_utf8_collate(Str1, Str2);
").



uc_collate(Str1, Str2, Compres) :-
    c_uc_collate(Str1, Str2, Res),
    (
        if   Res < 0
        then Compres = (<)

        else if Res = 0
        then Compres = (=)

        else Compres = (>)
    ).


uc_collate(A, B) = Res :-
    uc_collate(A, B, Res).


:- pragma foreign_proc("C",
    setlocale(IO0::di, IO1::uo),
    [promise_pure, will_not_call_mercury, tabled_for_io, thread_safe],
"    
    setlocale(LC_ALL, """");
    IO1 = IO0;
").

#! /bin/bash

for MD in *.md ; do
    BASIS=`basename "$MD" .md`
    echo -n "$MD"
    kramdown "$MD" > "$BASIS.html"
    echo ""
done
